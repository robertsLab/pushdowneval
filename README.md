# Push-Down Evaluation



## Overview

Entire push-down-trees testing environment used for the evaluation section of 
["Deterministic Self-Adjusting Tree Networks Using Rotor Walks"](https://arxiv.org/abs/2204.10754)
by Chen Avin, Marcin Bienkowski, Iosif Salem, Robert Sama, Stefan Schmid, and Paweł Schmidt. 

The methods used are listed in order in the simulation/main file.
Implemented in Python 3.9

## Dependencies
Matplotlib, pandas, pymongo, seaborn (detailed list to be found in requirements.txt)

## Working directory
- Needs to be the root directory (pushdowneval)
	- Can be set up in Pycharm in Run -> Edit Configurations -> Working directory
