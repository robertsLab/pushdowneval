import collections

from pushDown.treeStructure import print_tree


class half_move_opt():

    def __init__(self, tree):
        self.debug = False
        self.tree = tree
        self.rank2item_id = dict()  # item_id as key, rank is value
        self.rank_list2level = dict()  # level-number as key, queue of item_ids is value (sorted in ascending order, highest ranked item at end of queue)
        rank_incr = 1
        depth = 1
        for x in self.tree.index2item_id:  # iterate through all items
            self.rank2item_id[x] = rank_incr
            if rank_incr == 1:
                self.rank_list2level[0] = []
                self.rank_list2level[0].insert(0, x)
                rank_incr += 1
            else:

                # is a queue of items (sorted by rank) already registered for the given depth?
                if depth in self.rank_list2level:
                    self.rank_list2level[depth].append(x)
                else:
                    self.rank_list2level[depth] = []
                    self.rank_list2level[depth].insert(0, x)
                rank_incr += 1

                # rank_incr-1 == current index
                # pow(2, depth+1)-1  == first index of lower level
                # -> did we get to the next level?
                if (rank_incr - 1) >= (pow(2, depth + 1) - 1):
                    depth += 1
        if self.debug:
            print_tree(self.tree.root)
            print("Ranks:")
            for x in self.rank2item_id:
                print(str(x) + ': '+ str(self.rank2item_id[x]))
        # for testing
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nHalf move-opt-Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)
        if index == 0:
            return self.tree.root
        if index == -99:
            raise Exception("No Item for " + str(item_id) + " found")
        else:
            try:
                node = self.pull_up_item(item_id)
                if self.debug:
                    print_tree(self.tree.root)
                    print("Ranks:")
                    for x in self.rank2item_id:
                        print(str(x) + ': '+ str(self.rank2item_id[x]))
                return node
            except Exception as e:
                print(e.args)

    def pull_up_item(self, item_id):
        """
        pulls up accessed item (u) to desired location
        """
        depth_of_u = self.tree.get_depth_of_item(item_id)
        self.access_cost += depth_of_u
        replace_output = self.determine_replacing_item(depth_of_u)
        replacing_item = replace_output[0]
        replacing_depth = replace_output[1]
        if self.debug:
            print('item to replace ', replacing_item)

        # route to u, swap it up to root
        index_of_u = self.tree.get_index_of_item(item_id)
        path_to_u = self.tree.bit_string2index[index_of_u].copy()
        copy_of_path_to_u = path_to_u.copy()  # for later use (swapDownRoot)
        parent = self.tree.root
        child = self.get_next_child(parent, path_to_u)
        if not path_to_u:  # access on first level, just do one swap
            self.swap(parent, child)
        else:
            indexOfReplacing = self.tree.get_index_of_item(replacing_item)
            pathToReplacing = self.tree.bit_string2index[indexOfReplacing].copy()

            # result of new_paths=list[path_to_ancestor, path_to_replacing, path_to_u]
            new_paths = self.get_common_ancestor(pathToReplacing.copy(), copy_of_path_to_u.copy())

            # if u and v share an ancestor node, make that one to the reference node
            new_root = False
            if new_paths[0]:
                new_root = True
                child = self.get_next_child(parent, new_paths[0])
                while new_paths[0]:
                    parent = child
                    child = self.get_next_child(parent, new_paths[0])
                else:
                    parent = child
                    child = self.get_next_child(parent, new_paths[2].copy())
                new_parent = parent
                new_parent_copy = parent
                new_to_u = new_paths[2].copy()

            # replacing node is the ancestor node, just swap the sequence (replacing->u)
            if not new_paths[1]:
                new_paths[2].popleft()
                self.swap_defined_sequence(parent, child, new_paths[2].copy(), len(new_paths[2]))

            # replacing node is not ancestor
            else:
                if new_root:  # if there is ancestor, the ancestor is now reference node (treated like root)
                    child = self.get_next_child(parent, new_paths[2])
                    self.route_to_u_and_swap_up(parent, child, new_paths[2].copy(), item_id)
                else:
                    self.route_to_u_and_swap_up(parent, child, path_to_u, item_id)

                # swap the sequence to replaced item
                if new_root:
                    parent = new_parent
                    child = self.get_next_child(parent, new_paths[1])
                    self.swap_defined_sequence(parent, child, new_paths[1].copy(), len(new_paths[1]))
                else:
                    parent = self.tree.root
                    child = self.get_next_child(parent, pathToReplacing)
                    self.swap_defined_sequence(parent, child, pathToReplacing, len(pathToReplacing))

                # swap root-item to original u-node
                if new_root:
                    parent = new_parent_copy
                    child = self.get_next_child(parent, new_to_u)
                    self.swapDownRoot(parent, child, new_to_u.copy())
                else:
                    parent = self.tree.root
                    child = self.get_next_child(parent, copy_of_path_to_u)
                    self.swapDownRoot(parent, child, copy_of_path_to_u)

        # update ranks
        self.update_ranks_smaller_than(self.rank2item_id[item_id])
        self.rank2item_id[item_id] = 1
        
        # update rank-lists on interested levels
        self.rank_list2level[depth_of_u].remove(item_id)
        if self.debug:
            print('v in ', self.rank_list2level[depth_of_u])
            print('u in ', self.rank_list2level[replacing_depth])
        self.rank_list2level[depth_of_u] = self.sorted_insert(self.rank_list2level[depth_of_u], replacing_item)
        self.rank_list2level[replacing_depth] = self.sorted_insert(self.rank_list2level[replacing_depth], item_id)

        if self.debug:
            for x in self.rank_list2level:
                print(x, ' ', self.rank_list2level[x])
        return child

    # check if the paths have a common ancestor and returns the new paths to/from ancestor.
    # Otherwise, returns empty ancestor-path
    def get_common_ancestor(self, path_to_replacing, path_to_u) -> list:
        path_to_ancestor = collections.deque()
        while path_to_replacing:
            curr_repl = path_to_replacing.popleft()
            curr_u = path_to_u.popleft()
            if curr_repl == curr_u:
                path_to_ancestor.append(curr_repl)
            else:  # stop searching if there is a difference in paths, put diverse value in lists again
                path_to_replacing.insert(0, curr_repl)
                path_to_u.insert(0, curr_u)
                break
        self.adjust_cost += len(path_to_ancestor)               # cost: add len(ancestorPath) to adjust
        return [path_to_ancestor, path_to_replacing, path_to_u]

    def route_to_u_and_swap_up(self, parent, child, path_to_u, accessed_item):
        if not child:
            return
        if not path_to_u:
            follow_child = None
        else:
            follow_child = self.get_next_child(child, path_to_u)
        follow_parent = child
        self.route_to_u_and_swap_up(follow_parent, follow_child, path_to_u, accessed_item)
        self.swap(parent, child)

    def swapDownRoot(self, parent, child, path_to_u):
        while (True):
            self.swap(parent, child)
            new_child = self.get_next_child(child, path_to_u)
            parent = child
            child = new_child
            if not path_to_u:
                self.swap(parent, child)
                break

    # Function to insert element
    def sorted_insert(self, rank_list, item_id):
        if not rank_list:
            rank_list = []
            rank_list.append(item_id)
        else:
            # Searching for position of item with rank higher than insertingItem
            for i in range(len(rank_list)):
                if self.rank2item_id[rank_list[i]] > self.rank2item_id[item_id]:  # i = position
                    break

            # Inserting n in the list
            rank_list = rank_list[:i] + [item_id] + rank_list[i:]
        return rank_list

    def update_ranks_smaller_than(self, r):
        for x in self.rank2item_id:
            if self.rank2item_id[x] < r:
                self.rank2item_id[x] += 1

    def determine_replacing_item(self, depth) -> list:
        replacing_depth = (int)(depth / 2)
        self.adjust_cost += replacing_depth

        # retrieve and remove last item of list of level,
        # must be the highest ranked one since list is sorted by rank
        replacing_item_id = self.rank_list2level[replacing_depth].pop()
        return [replacing_item_id, replacing_depth]

    def get_next_child(self, parent, path):
        child = None
        if path.popleft() == 1 and parent.right:
            child = parent.right
        else:                   # if right child not available, check if there's a left child
            child = parent.left

        if child is None:
            raise Exception('No child_node for parent ', str(parent.get_guest()), ' found!')
            
        # no left or right children available --> return None
        return child

    def swap(self, one, two):
        if self.debug:
            print("swapping " + str(one.guest) + " WITH " + str(two.guest))
        temp = one.get_guest()
        one.guest = two.guest
        two.guest = temp
        self.tree.index2item_id[one.guest.id] = one.get_index()
        self.tree.index2item_id[two.guest.id] = two.get_index()
        self.swaps += 1

    def swap_defined_sequence(self, parent, child, path, k):
        self.swap(parent, child)
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            self.swap_defined_sequence(follow_parent, follow_child, path, k - 1)

        # swap the accessed item up to the root. Last original swap not to be reversed
        if k > 0:
            self.swap(parent, child)


