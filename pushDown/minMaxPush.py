import datetime
import copy
from pushDown.treeStructure import *

# min and max push implementations (same concept but different implementation)

# same as minPush in functionality
# but works with ranks instead of time
class maxPush():
    def __init__(self, tree):
        self.debug = False
        self.tree = tree
        self.rank2item_id = dict()  # item_id as key, rank is value
        self.rank_list2level = dict()  # level-number as key, queue of item_ids is value (sorted in ascending order, highest ranked item at end of queue)
        rank_incr = 1
        depth = 1
        for x in self.tree.index2item_id:  # iterate through all items
            self.rank2item_id[x] = rank_incr  # assign rank to item
            if rank_incr == 1:
                self.rank_list2level[0] = []
                self.rank_list2level[0].insert(0, x)
                rank_incr += 1
            else:

                # is a queue of items (sorted by rank) already registered for the given depth?
                if depth in self.rank_list2level:
                    self.rank_list2level[depth].append(x)
                else:
                    self.rank_list2level[depth] = []
                    self.rank_list2level[depth].insert(0, x)
                rank_incr += 1

                # rank_incr-1 == current index
                # pow(2, depth+1)-1  == first index of lower level
                # did we get to the next level?
                if (rank_incr - 1) >= (pow(2, depth + 1) - 1):
                    depth += 1
        self.set_of_ws = None
        self.copy_of_ws = None  # copy of Ws to use in order to update ranks after access
        self.affected_list = collections.deque()  # used for updating rank-lists in correct order
        self.u_pulled_up = False
        if self.debug:
            print_tree(self.tree.root)
            print("Ranks:")
            for x in self.rank2item_id:
                print(str(x) + ': ' + str(self.rank2item_id[x]))

        # for testing
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nMax Push-Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)
        if index == 0:
            return self.tree.root
        if index == -99:
            raise Exception("No Item for " + str(item_id) + " found")
        else:
            try:
                node = self.pull_up_item(item_id)
                self.u_pulled_up = False
                self.affected_list.clear()
                if self.debug:
                    print_tree(self.tree.root)
                    print("Ranks:")
                    for x in self.rank2item_id:
                        print(str(x) + ': ' + str(self.rank2item_id[x]))
                return node
            except Exception as e:
                print(e.args)

    def pull_up_item(self, item_id):
        """ pulls up accessed item (u) to root """
        depth = self.tree.get_depth_of_item(item_id)
        self.access_cost += depth
        if self.debug:
            print("AccessCost = " + str(self.access_cost))
        self.set_of_ws = self.getSetOfW(depth)
        self.copy_of_ws = self.set_of_ws.copy()
        self.adjust_cost += depth
        parent_node = self.tree.root

        # accessed item (u) is on first level, simply swap it with the root item and exit
        if not self.set_of_ws:
            if self.debug:
                print("NO Ws")
            child_node = self.getNodeHostingItem(parent_node, item_id)  # pick u.host, swap u to root
            self.rank_list2level[0].remove(parent_node.get_guest().id)
            self.rank_list2level[1].remove(child_node.get_guest().id)
            self.rank_list2level[0].insert(0, child_node.get_guest().id)
            self.rank_list2level[1] = self.sorted_insert(self.rank_list2level[1], parent_node.get_guest().id)
            self.swap(parent_node, child_node)
            self.rank2item_id[parent_node.get_guest().id] = 1
            self.rank2item_id[child_node.get_guest().id] = 2
            if self.debug:
                for x in self.rank_list2level:
                    print(x, ' ', self.rank_list2level[x])
            return self.tree.root

        # there is at least one w, start algorithm
        self.affected_list.append(self.tree.root.guest.id)
        for x in self.set_of_ws:
            self.affected_list.append(x)
        self.affected_list.append(item_id)
        if self.debug:
            print("affected nodes:")
            for x in self.affected_list:
                print(x)

        # procedure for first w = w1
        searched_w_item = self.set_of_ws.popleft()
        child_node = self.getNodeHostingItem(parent_node, searched_w_item)  # pick w1.host
        if not self.set_of_ws:
            if self.debug:
                print("one W only, = " + str(child_node.get_guest().id))
            # case where there is only one w --> u is at depth = 2
            if child_node.left and child_node.left.get_guest().id == item_id:
                self.swap(child_node, child_node.left)
                self.swap(parent_node, child_node)  # u at root
            elif child_node.right and child_node.right.get_guest().id == item_id:
                self.swap(child_node, child_node.right)
                self.swap(parent_node, child_node)  # u at root
            else:
                if self.debug:
                    print("u not connected to W1")

                # u not connected to w1 -> swap w1 at root first and search for u
                self.swap(parent_node, child_node)
                searched_index = self.tree.get_index_of_item(item_id)
                path = self.tree.bit_string2index[searched_index].copy()
                child_node = self.get_next_child(parent_node, path)
                self.swap_defined_sequence(parent_node, child_node, path, len(path))  # swap w1 with u
            self.update_rank_lists_until(depth=depth, accessed_item=item_id)
            return self.tree.root

        # there is at least a w2 --> iterate to w1 and look if w1 is connected with further Ws
        searched_index = self.tree.index2item_id[searched_w_item]
        tempset_of_ws = self.set_of_ws.copy()
        self.startFirstW(child_node, searched_index, tempset_of_ws, item_id)  # pulls up some w at root

        # some kind of w(>=1) is at root, swap with further w+1
        while self.set_of_ws and not self.u_pulled_up:
            next_w_item = self.set_of_ws.popleft()
            searched_index = self.tree.get_index_of_item(next_w_item)
            path = self.tree.bit_string2index[searched_index].copy()
            child_node = self.get_next_child(parent_node, path)
            tempset_of_ws = self.set_of_ws.copy()
            self.swapSequenceTillParentOfWandCheck(parent_node, child_node, path, len(path), tempset_of_ws, item_id)

        # case where last w was not connected to u
        if not self.u_pulled_up:
            # now w(u.dep-1) is at root. Do last sequenced swap to pull up u at root and push down w(u.dep-1) at u.host
            searched_index = self.tree.get_index_of_item(item_id)
            path = self.tree.bit_string2index[searched_index].copy()
            child_node = self.get_next_child(parent_node, path)
            self.swap_defined_sequence(parent_node, child_node, path, len(path))
        self.update_rank_lists_until(depth=depth, accessed_item=item_id)
        return self.tree.root

    def getNodeHostingItem(self, parent_node, item_id) -> Node:
        searched_index = self.tree.get_index_of_item(item_id)
        path = self.tree.bit_string2index[searched_index].copy()
        return self.get_next_child(parent_node, path)

    def startFirstW(self, w, searched_index, tempset_of_ws, accessed_item):

        # check if w1 is reached
        if not w.get_index() == searched_index:
            raise Exception("W1 should have been reached here!")
        w2item = tempset_of_ws.popleft()
        child = None
        if w.left.get_guest().id == w2item:
            child = w.left
            self.set_of_ws.popleft()
        elif w.right.get_guest().id == w2item:
            child = w.right
            self.set_of_ws.popleft()
        else:

            # no connection, swap w1 at root
            self.swap(self.tree.root, w)
            return

        # w1 connected to w2
        if tempset_of_ws:

            # check if w2 furtherly connected and swap furthest W to w1.host
            self.iterateToFurthestWandSwapBack(w, child, tempset_of_ws, accessed_item)
        else:
            self.swap(w, child)  # no w3 there -> swap w2 to w.host
        self.swap(self.tree.root, w)  # swap w2 (or furthest found w) to root

    def swapSequenceTillParentOfWandCheck(self, parent, child, path, k, tempset_of_ws, accessed_item):
        """swaps w from root down to parent_node of w+1
        there it checks whether w+1 has further children to swap before pulling the furthest w-item up to the root
        to do this, a copy of set_of_ws object is passed as argument
        furtherly takes the accessed_item (u) as argument, in case last W is connected to it
        """
        self.swap(parent, child)
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            if not path:

                # no further steps -> pushed down w-node (follow_parent) is now parent of w+1 (follow_child)
                new_child = follow_child
                iterateChild = None
                
                # starting from w+1, check if there are any interesting connected nodes
                if tempset_of_ws and new_child.left:
                    nextW = tempset_of_ws.popleft()

                    # check if w+1 has further connections to Ws
                    if new_child.left.get_guest().id == nextW:
                        iterateChild = new_child.left
                        self.set_of_ws.popleft()  # since we found a connection to W, pop w from original queue too
                    elif new_child.right and new_child.right.get_guest().id == nextW:
                        iterateChild = new_child.right
                        self.set_of_ws.popleft()
                elif new_child.left and new_child.left.get_guest().id == accessed_item:
                    self.u_pulled_up = True
                    iterateChild = new_child.left
                elif new_child.right and new_child.right.get_guest().id == accessed_item:
                    self.u_pulled_up = True
                    iterateChild = new_child.right

                # iterate through chain of connected Ws and swap last W (or u) to position of child
                self.iterateToFurthestWandSwapBack(new_child, iterateChild, tempset_of_ws, accessed_item)
            self.swapSequenceTillParentOfWandCheck(follow_parent, follow_child, path, k - 1, tempset_of_ws,
                                                   accessed_item)

        # swap the accessed item up to the root. Last original swap not to be reversed
        if k > 0:
            self.swap(parent, child)

    def iterateToFurthestWandSwapBack(self, parent, child, tempset_of_ws, accessed_item):
        """ iterates from current w-node to the last connected w-node (or, eventually, u)
        once arrived at end of chain, swaps the iterated sequence
        """

        # parent is a w and has connection to child w+1
        if not child:  # no Ws (or u) left, no iteration needed
            return
        follow_child = None
        if tempset_of_ws and child.left:
            nextW = tempset_of_ws.popleft()

            # check if w+1 has further connections to Ws
            if child.left.get_guest().id == nextW:
                follow_child = child.left
                self.set_of_ws.popleft()  # since we found a connection to W, pop w from original queue too
            elif child.right and child.right.get_guest().id == nextW:
                follow_child = child.right
                self.set_of_ws.popleft()
        elif child.left and child.left.get_guest().id == accessed_item:
            self.u_pulled_up = True
            follow_child = child.left
        elif child.right and child.right.get_guest().id == accessed_item:
            self.u_pulled_up = True
            follow_child = child.right
        follow_parent = child
        self.iterateToFurthestWandSwapBack(follow_parent, follow_child, tempset_of_ws, accessed_item)

        # second part of recursive method -> swaps iterated items up to origin of check (w) == last parent
        self.swap(parent, child)

    def swap_defined_sequence(self, parent, child, path, k):
        self.swap(parent, child)
        if (path):
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            self.swap_defined_sequence(follow_parent, follow_child, path, k - 1)

        # swap the accessed item up to the root. Last original swap not to be reversed
        if (k > 0):
            self.swap(parent, child)

    def getSetOfW(self, depth):
        """
        returns queue of leastRU items w that have to be swapped with next-level w. There will be u.depth-1 items
        If all items on level have been accessed at same time, the first item of level is chosen
        Order: item on first level is leftmost in queue
        Return empty Queue if accessed item is on first level
        """
        currentLevel = 1
        lrus = collections.deque()
        if depth == currentLevel:
            return lrus

        # for each level, get the highest ranked item (which is the least recently accessed)
        # to do so, retrieve and remove last item of list of level,
        # must be the highest ranked one since list is sorted by rank
        copy_of_list = copy.deepcopy(self.rank_list2level)
        while not (currentLevel == depth):
            replacing_item_id = copy_of_list[currentLevel].pop()
            currentLevel += 1
            lrus.append(replacing_item_id)  # put item of chosen index in queue
        return lrus

    def update_ranks_smaller_than(self, r):
        for x in self.rank2item_id:
            if self.rank2item_id[x] < r:
                self.rank2item_id[x] += 1

    def update_rank_lists_until(self, depth, accessed_item):
    
        # update ranks
        self.update_ranks_smaller_than(self.rank2item_id[accessed_item])
        self.rank2item_id[accessed_item] = 1
        curr_level = 0
        while curr_level <= depth:
            curr_item = self.affected_list.popleft()  # take affected (w)-item for each level
            if not self.affected_list:  # we got to the last item, which is inserted at level 0
                if self.debug:
                    print("Inserting " + str(curr_item) + " to root ")
                self.rank_list2level[curr_level].remove(curr_item)
                self.rank_list2level[0].insert(0, curr_item)
                return
            if self.debug:
                print("removing " + str(curr_item) + " from depth " + str(curr_level))
                for x in self.rank_list2level:
                    print(x, ' ', self.rank_list2level[x])
            # update rank-lists on interested levels
            self.rank_list2level[curr_level].remove(curr_item)
            self.rank_list2level[curr_level + 1] = self.sorted_insert(self.rank_list2level[curr_level + 1], curr_item)
            curr_level += 1

    # Function to insert element FROM https://www.geeksforgeeks.org/python-program-to-insert-an-element-into-sorted-list/
    def sorted_insert(self, rank_list, item_id):
        if not rank_list:
            rank_list = []
            rank_list.append(item_id)
        else:
            # Searching for position of item with rank higher than insertingItem
            for i in range(len(rank_list)):
                if self.rank2item_id[rank_list[i]] > self.rank2item_id[item_id]:  # i = position
                    break

            # Inserting n in the list
            rank_list = rank_list[:i] + [item_id] + rank_list[i:]
        return rank_list

    def swap(self, one, two):
        if self.debug:
            print("swapping " + str(one.guest) + " WITH " + str(two.guest))
        temp = one.get_guest()
        one.guest = two.guest
        two.guest = temp
        self.tree.index2item_id[one.guest.id] = one.get_index()
        self.tree.index2item_id[two.guest.id] = two.get_index()
        self.swaps += 1

    def get_next_child(self, parent, path):
        child = None
        if (path.popleft() == 1 and parent.right):
            child = parent.right
        elif (parent.left):  # if right child not available, check if there's a left child
            child = parent.left

        # no left or right children available --> return None
        return child


class minPush():
    def __init__(self, tree):
        self.debug = False
        self.tree = tree
        self.timeStampToItem = dict()
        self.set_of_ws = collections.deque()
        self.u_pulled_up = False
        time = datetime.datetime.now()
        self.microSecondIncr = 1

        # root item will have youngest timestamp, left child older than root, right child older than left
        for x in self.tree.index2item_id:
            self.timeStampToItem[x] = time - datetime.timedelta(
                    microseconds=self.microSecondIncr)
            self.microSecondIncr += 1
        self.microSecondIncr = 1

        # for testing
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nMin Push-Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)
        if index == 0:
            return self.tree.root
        if index == -99:
            raise Exception("No Item for " + str(item_id) + " found")
        else:
            try:
                node = self.pull_up_item(item_id)

                # make sure the timeStamp is incremented by at least one microSec compared to last accessed item
                self.timeStampToItem[item_id] = datetime.datetime.now() + datetime.timedelta(
                    microseconds=self.microSecondIncr)
                self.microSecondIncr += 1
                self.u_pulled_up = False
                if self.debug:
                    print_tree(self.tree.root)
                return node
            except Exception as e:
                print(e.args)

    """
    pulls up accessed item (u) to root
    """

    def pull_up_item(self, item_id):
        depth = self.tree.get_depth_of_item(item_id)
        self.access_cost += depth
        if self.debug:
            print("AccessCost = " + str(self.access_cost))
        self.set_of_ws = self.getSetOfW(depth)
        self.adjust_cost += depth
        if self.debug:
            print("Selected Ws:")
            for x in self.set_of_ws:
                print(x)
        parent_node = self.tree.root

        # accessed item (u) is on first level, simply swap it with the root item and exit
        if not self.set_of_ws:
            child_node = self.getNodeHostingItem(parent_node, item_id)  # pick u.host, swap u to root
            self.swap(parent_node, child_node)
            return self.tree.root

        # procedure for first w = w1
        searched_w_item = self.set_of_ws.popleft()
        child_node = self.getNodeHostingItem(parent_node, searched_w_item)  # pick w1.host
        if not self.set_of_ws:
            if self.debug:
                print("one W only, " + "currNode = " + str(child_node.get_guest().id))
                
            # case where there is only one w --> u is at depth = 2
            if child_node.left and child_node.left.get_guest().id == item_id:
                self.swap(child_node, child_node.left)
                self.swap(parent_node, child_node)  # u at root
            elif child_node.right and child_node.right.get_guest().id == item_id:
                self.swap(child_node, child_node.right)
                self.swap(parent_node, child_node)  # u at root
            else:
                if self.debug:
                    print("u not connected to W1")

                # u not connected to w1 -> swap w1 at root first and search for u
                self.swap(parent_node, child_node)
                searched_index = self.tree.get_index_of_item(item_id)
                path = self.tree.bit_string2index[searched_index].copy()
                child_node = self.get_next_child(parent_node, path)
                self.swap_defined_sequence(parent_node, child_node, path, len(path))  # swap w1 with u
            return self.tree.root

        # there is at least a w2 --> iterate to w1 and look if w1 is connected with further Ws
        searched_index = self.tree.index2item_id[searched_w_item]
        tempset_of_ws = self.set_of_ws.copy()
        self.startFirstW(child_node, searched_index, tempset_of_ws, item_id)  # pulls up some w at root

        # some kind of w(>=1) is at root, swap with further w+1
        while self.set_of_ws and not self.u_pulled_up:
            next_w_item = self.set_of_ws.popleft()
            searched_index = self.tree.get_index_of_item(next_w_item)
            path = self.tree.bit_string2index[searched_index].copy()
            child_node = self.get_next_child(parent_node, path)
            tempset_of_ws = self.set_of_ws.copy()
            self.swapSequenceTillParentOfWandCheck(parent_node, child_node, path, len(path), tempset_of_ws, item_id)

        # case where last w was not connected to u
        if not self.u_pulled_up:
        
            # now w(u.dep-1) is at root. Do last sequenced swap to pull up u at root and push down w(u.dep-1) at u.host
            searched_index = self.tree.get_index_of_item(item_id)
            path = self.tree.bit_string2index[searched_index].copy()
            child_node = self.get_next_child(parent_node, path)
            self.swap_defined_sequence(parent_node, child_node, path, len(path))
        return self.tree.root

    def getNodeHostingItem(self, parent_node, item_id) -> Node:
        searched_index = self.tree.get_index_of_item(item_id)
        path = self.tree.bit_string2index[searched_index].copy()
        return self.get_next_child(parent_node, path)

    def startFirstW(self, w, searched_index, tempset_of_ws, accessed_item):

        # check if w1 is reached
        if not w.get_index() == searched_index:
            raise Exception("W1 should have been reached here!")
        w2item = tempset_of_ws.popleft()
        child = None
        if w.left.get_guest().id == w2item:
            child = w.left
            self.set_of_ws.popleft()
        elif w.right.get_guest().id == w2item:
            child = w.right
            self.set_of_ws.popleft()
        else:

            # no connection, swap w1 at root
            self.swap(self.tree.root, w)
            return

        # w1 connected to w2
        if tempset_of_ws:

            # check if w2 furtherly connected and swap furthest W to w1.host
            self.iterateToFurthestWandSwapBack(w, child, tempset_of_ws, accessed_item)
        else:
            self.swap(w, child)  # no w3 there -> swap w2 to w.host
        self.swap(self.tree.root, w)  # swap w2 (or furthest found w) to root

    def swapSequenceTillParentOfWandCheck(self, parent, child, path, k, tempset_of_ws, accessed_item):
        """swaps w from root down to parent_node of w+1
        there it checks whether w+1 has further children to swap before pulling the furthest w-item up to the root
        to do this, a copy of set_of_ws object is passed as argument
        furtherly takes the accessed_item (u) as argument, in case last W is connected to it
        """
        self.swap(parent, child)
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            if not path:

                # no further steps -> pushed down w-node (follow_parent) is now parent of w+1 (follow_child)
                new_child = follow_child
                iterateChild = None
                
                # starting from w+1, check if there are any interesting connected nodes
                if tempset_of_ws and new_child.left:
                    nextW = tempset_of_ws.popleft()

                    # check if w+1 has further connections to Ws
                    if new_child.left.get_guest().id == nextW:
                        iterateChild = new_child.left
                        self.set_of_ws.popleft()  # since we found a connection to W, pop w from original queue too
                    elif new_child.right and new_child.right.get_guest().id == nextW:
                        iterateChild = new_child.right
                        self.set_of_ws.popleft()
                elif new_child.left and new_child.left.get_guest().id == accessed_item:
                    self.u_pulled_up = True
                    iterateChild = new_child.left
                elif new_child.right and new_child.right.get_guest().id == accessed_item:
                    self.u_pulled_up = True
                    iterateChild = new_child.right

                # iterate through chain of connected Ws and swap last W (or u) to position of child
                self.iterateToFurthestWandSwapBack(new_child, iterateChild, tempset_of_ws, accessed_item)
            self.swapSequenceTillParentOfWandCheck(follow_parent, follow_child, path, k - 1, tempset_of_ws,
                                                   accessed_item)

        # swap the accessed item up to the root. Last original swap not to be reversed
        if k > 0:
            self.swap(parent, child)

    def iterateToFurthestWandSwapBack(self, parent, child, tempset_of_ws, accessed_item):
        """ iterates from current w-node to the last connected w-node (or, eventually, u)
            once arrived at end of chain, swaps the iterated sequence
        """

        # parent is a w and has connection to child w+1
        if not child:  # no Ws (or u) left, no iteration needed
            return
        follow_child = None
        if tempset_of_ws and child.left:
            nextW = tempset_of_ws.popleft()

            # check if w+1 has further connections to Ws
            if child.left.get_guest().id == nextW:
                follow_child = child.left
                self.set_of_ws.popleft()  # since we found a connection to W, pop w from original queue too
            elif child.right and child.right.get_guest().id == nextW:
                follow_child = child.right
                self.set_of_ws.popleft()
        elif child.left and child.left.get_guest().id == accessed_item:
            self.u_pulled_up = True
            follow_child = child.left
        elif child.right and child.right.get_guest().id == accessed_item:
            self.u_pulled_up = True
            follow_child = child.right
        follow_parent = child
        self.iterateToFurthestWandSwapBack(follow_parent, follow_child, tempset_of_ws, accessed_item)

        # second part of recursive method -> swaps iterated items up to origin of check (w) == last parent
        self.swap(parent, child)

    def swap_defined_sequence(self, parent, child, path, k):
        self.swap(parent, child)
        if (path):
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            self.swap_defined_sequence(follow_parent, follow_child, path, k - 1)

        # swap the accessed item up to the root. Last original swap not to be reversed
        if (k > 0):
            self.swap(parent, child)

    def getSetOfW(self, depth):
        """
        returns queue of leastRU items w that have to be swapped with next-level w. There will be u.depth-1 items
        If all items on level have been accessed at same time, the first item of level is chosen
        Order: item on first level is leftmost in queue
        Return empty Queue if accessed item is on first level
        """
        currentLevel = 1
        lrus = collections.deque()
        if (depth == currentLevel):
            return lrus
        inv_map = {v: k for k, v in self.tree.index2item_id.items()}  # maps items to indexes (inv_map[index] = item)
        while not (currentLevel == depth):
            currentIndex = pow(2, currentLevel) - 1  # pow(2, currentLevel)-1 == first index of actual level
            currentItem = inv_map[currentIndex]
            oldestIndex = currentIndex
            oldestTime = self.timeStampToItem[currentItem]  # oldest time first set for the first item on level
            while (currentIndex < pow(2,
                                      currentLevel + 1) - 1):  # pow(2, currentLevel+1)-1  == first index of lower level
                currentItem = inv_map[currentIndex]
                if (self.timeStampToItem[currentItem] < oldestTime):
                    oldestTime = self.timeStampToItem[currentItem]
                    oldestIndex = currentIndex
                currentIndex += 1
            currentLevel += 1
            lrus.append(inv_map[oldestIndex])  # put item of chosen index in queue
        return lrus

    def swap(self, one, two):
        if self.debug:
            print("swapping " + str(one.guest) + " WITH " + str(two.guest))
        temp = one.get_guest()
        one.guest = two.guest
        two.guest = temp
        self.tree.index2item_id[one.guest.id] = one.get_index()
        self.tree.index2item_id[two.guest.id] = two.get_index()
        self.swaps += 1

    def get_next_child(self, parent, path):
        child = None
        if (path.popleft() == 1 and parent.right):
            child = parent.right
        elif (parent.left):  # if right child not available, check if there's a left child
            child = parent.left

        # no left or right children available --> return None
        return child
