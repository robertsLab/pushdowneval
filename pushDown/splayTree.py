# copied from https://github.com/void-/splay/blob/master/splay_tree.py
# and adapted on 05.04.2021
class SplayTree(object):
    """Splay Tree object.
    A splay tree is a self balancing binary search tree with operations that run
    in O(log(n)) amortized time, where n is the number of entries in the tree.
    This particular implementation uses a dictionary interface; it may be
    extended to use a key only interface however.
    Member Variables:
      _size the number of entries in this splay tree.
      root the root of this tree; an empty tree has a value of None.
    """

    def __init__(self):
        """Initialize an empty splay tree object."""

        self._size = 0
        self.root = None
        self.access_cost = 0
        self.adjust_cost = 0
        self.swaps = 0

    def __len__(self):
        """Return the size(number of entries) of the splay tree."""

        return self._size

    def reset_costs(self):
        self.access_cost = 0
        self.adjust_cost = 0
        self.swaps = 0

    def insert(self, key, value):
        """Insert an item into the splay tree, increasing its size.
        Keys will be inserted in O(log(n)) amortized time and then splayed to the
        root of the tree. Duplicates are **not** allowed in the sense that only
        one copy of a key is allowed in the tree at a time. Further insertions
        with that key will overwrite previous keys.
        """
        if self.root:
            node = self.binaryHelper(key, self.root)
            if key == node.key:  # Replace a duplicate key
                node._key = key
                node._value = value
                return
            elif key < node.key:
                node.left = TreeNode(key, value, node)
                node = node.left
            elif key > node.key:
                node.right = TreeNode(key, value, node)
                node = node.right
            self.splay(node)
        else:
            self.root = TreeNode(key, value)
        self._size += 1

    def insertHelper(self, key, node):
        """Insert an item into the splay tree given a node.
        Recursive helper function that inserts keys into a binary tree. The node
        that the key is inserted to is returned.
        """
        if key < node.entry:
            if node.left:
                return self.insertHelper(key, node.left)
            else:
                node.left = TreeNode(key, parent=node)
                return node
        else:
            if node.right:
                return self.insertHelper(key, node.right)
            else:
                node.right = TreeNode(key, parent=node)
                return node

    def find(self, key):
        """Return the value that corresponds to the given key.
        Search the tree for the given key and return its corresponding value in
        O(log(n)) amortized time. If the key is not in this tree, return None.
        The node that the search ends on is splayed to the root of the tree. No
        duplicates are allowed in this implementation.
        """
        if self.root:
            node = self.binaryHelper(key, self.root)
            self.splay(node)  # Splay the found node to the root
            if node.key == key:
                return node.value

    def splay(self, node):
        """Splay a node up to the root.
        Mutate this splay tree so that the given node becomes the root of the tree.
        There are three named cases:
        zig-zig:
          The node is a right child of a right child OR a left of a left.
          In this case, rotate up through the parent, then through the grandparent.
        zig-zag:
          The node is a right child of a left child OR a left of a right.
          In this case, rotate the parent up through the grandparent, then rotate
          the node up through the parent.
        zig(base-case):
          The node is either a right child or a left child of the root. Rotate up.
        When the given node is the root of the tree, stop recursion. If node is
        None, return.
        """
        if not node or node is self.root:
            return
        elif node.parent is self.root:  # Zig
            if node.isLeftChild:
                self.access_cost += 1
                return self.rotateRight(node)
            elif node.isRightChild:
                self.access_cost += 1
                return self.rotateLeft(node)
        # right left zig-zag
        elif node.isRightChild and node.parent.isLeftChild:
            self.rotateLeft(node)
            self.rotateRight(node)
        # left right zig-zag
        elif node.isLeftChild and node.parent.isRightChild:
            self.rotateRight(node)
            self.rotateLeft(node)
        # left zig-zig
        elif node.isLeftChild and node.parent.isLeftChild:
            self.rotateRight(node.parent)
            self.rotateRight(node)
        # right zig-zig
        elif node.isRightChild and node.parent.isRightChild:
            self.rotateLeft(node.parent)
            self.rotateLeft(node)
        self.access_cost += 2
        return self.splay(node)  # recurse

    def rotateRight(self, node):
        """Rotate a given node right in the tree.
              P                         n
             / \    rotateRight()      / \
            n   ^   ------------>     ^   P
           / \ /C\                   /A\ / \
          ^  ^                           ^  ^
         /A\/B\                         /B\/C\
        >>> s = SplayTree()
        >>> a = TreeNode(3,0)
        >>> b = TreeNode(2,1)
        >>> b.parent = a
        >>> c = TreeNode(1,2)
        >>> c.parent = b
        >>> s.root = a
        >>> s.root.left = b
        >>> s.root.left.left = c
        >>> s.rotateRight(b)
        >>> s.root is b
        True
        >>> s.root.left is c
        True
        >>> s.root.right is a
        True
        """
        node.parent.left = node.right   # swap #1
        if node.right:
            node.right.parent = node.parent
            self.swaps += 1
        node.right = node.parent                # 2
        node.parent = node.parent.parent        # 3
        node.right.parent = node                # 4
        self.swaps += 4
        if node.parent:  # If node's new parent is not None
            if node.parent.left is node.right:
                node.parent.left = node
                self.swaps += 1
            elif node.parent.right is node.right:
                node.parent.right = node
                self.swaps += 1
        else:
            self.root = node

    def rotateLeft(self, node):
        """Rotate a given node left in the tree.
           P                           n
          / \        rotateLeft()     / \
         ^   n       ---------->     P   ^
        /A\ / \                     / \ /C\
            ^  ^                   ^  ^
           /B\/C\                 /A\/B\
        """
        node.parent.right = node.left
        if node.left:
            node.left.parent = node.parent
            self.swaps += 1
        node.left = node.parent
        node.parent = node.parent.parent
        node.left.parent = node
        self.swaps += 4
        if node.parent:  # If node's new parent is not None
            if node.parent.left is node.left:
                node.parent.left = node
                self.swaps += 1
            elif node.parent.right is node.left:
                node.parent.right = node
                self.swaps += 1
        else:
            self.root = node
            self.swaps += 1

    def __contains__(self, key):
        """Determine if a given key is within the tree. Wrapper for find().
        This function returns false negatives if the entries in the tree are None.
        """

        return self.find(key) is not None

    def remove(self, key):
        """Remove an item from the splay tree.
        Given a key, it will be removed in O(log(n)) amortized time and its parent
        will be splayed to the root of the tree. If the operation is successful,
        the size of the tree will decrease be one and the value of the key will be
        returned, otherwise, a value of None will be returned. Calling remove() on
        a duplicate key will result in an arbitrary key being removed.
        """

        if not self.root:  # nothing to remove
            return
        remove = self.binaryHelper(key, self.root)
        splayMe = None
        # remove is not None
        if remove.key != key:  # node is not in the tree
            splayMe = remove
        elif remove is self.root:
            assert not remove.parent
            if not remove.left and not remove.right:  # 0 children
                self.root = None
            elif remove.left and not remove.right:  # left child only
                self.root = remove.left
                remove.left.parent = None
                remove.left = None
            elif remove.right and not remove.left:  # right child only
                self.root = remove.right
                remove.right.parent = None
                remove.right = None
            elif remove.right and remove.left:  # both children
                replace = self.minNode(remove.right)
                assert replace
                assert not replace.left

                if replace is remove.right:
                    replace.left = remove.left
                    replace.left.parent = replace
                    remove.left = None
                    replace.parent = None
                    remove.right = None
                    self.root = replace
                else:
                    assert replace.isLeftChild
                    replace.left = remove.left
                    replace.left.parent = replace
                    remove.left = None
                    if replace.right:
                        replace.right.parent = replace.parent
                        replace.parent.left = replace.right
                    else:
                        replace.parent.left = None
                    replace.parent = None
                    remove.right.parent = replace
                    replace.right = remove.right
                    remove.right = None
                    self.root = replace

        else:  # handle not root
            assert remove.parent
            if not remove.left and not remove.right:  # 0 children
                if remove.isLeftChild:
                    remove.parent.left = None
                elif remove.isRightChild:
                    remove.parent.right = None
                splayMe = remove.parent
                remove.parent = None
            elif remove.left and not remove.right:  # left child only
                if remove.isLeftChild:
                    remove.parent.left = remove.left
                    remove.left.parent = remove.parent
                    remove.left = None
                    splayMe = remove.parent
                    remove.parent = None
                elif remove.isRightChild:
                    remove.parent.right = remove.left
                    remove.left.parent = remove.parent
                    remove.left = None
                    splayMe = remove.parent
                    remove.parent = None
            elif remove.right and not remove.left:  # right child only
                if remove.isLeftChild:
                    remove.parent.left = remove.right
                    remove.right.parent = remove.parent
                    remove.right = None
                    splayMe = remove.parent
                    remove.parent = None
                elif remove.isRightChild:
                    remove.parent.right = remove.right
                    remove.right.parent = remove.parent
                    remove.right = None
                    splayMe = remove.parent
                    remove.parent = None
            elif remove.left and remove.right:  # both children
                splayMe = replace = self.minNode(remove.right)
                assert replace
                assert not replace.left

                if remove.isLeftChild:
                    if replace is remove.right:
                        replace.left = remove.left
                        replace.left.parent = replace
                        remove.left = None
                        remove.parent.left = replace
                        replace.parent = remove.parent
                        remove.parent = None
                    else:
                        assert replace.isLeftChild
                        replace.left = remove.left
                        replace.left.parent = replace
                        remove.left = None
                        if replace.right:
                            replace.parent.left = replace.right
                            replace.right.parent = replace.parent
                        else:
                            replace.parent.left = None
                        replace.right = remove.right
                        replace.right.parent = replace
                        remove.right = None
                        replace.parent = remove.parent
                        replace.parent.left = replace
                        remove.parent = None
                elif remove.isRightChild:
                    if replace is remove.right:
                        replace.left = remove.left
                        replace.left.parent = replace
                        remove.left = None
                        remove.parent.right = replace
                        replace.parent = remove.parent
                        remove.parent = None
                    else:
                        assert replace.isLeftChild
                        replace.left = remove.left
                        replace.left.parent = replace
                        remove.left = None
                        if replace.right:
                            replace.right.parent = replace.parent
                            replace.parent.left = replace.right
                        else:
                            replace.parent.left = None
                        replace.right = remove.right
                        replace.right.parent = replace
                        remove.right = None
                        replace.parent = remove.parent
                        replace.parent.right = replace
                        remove.parent = None
        self.splay(splayMe)
        self._size -= 1

    def minNode(self, node):
        """Return the node that contains the minimum key.
        Helper function that returns the node with the minimum key in a tree given
        the root of that tree. If the given node is None, return None.
        """
        if not node or not node.left:
            return node
        return self.minNode(node.left)

    def maxNode(self, node):
        """Return the node that contains the maximum key.
        Helper function that returns the node with the maximum key in a tree given
        the root of that tree.
        """
        if not node or not node.right:
            return node
        return self.maxNode(node.right)

    def binaryHelper(self, key, node):
        """Find a node that is *right* for the given key.
        Helper function that returns the node that suits the key. If the key is not
        currently in the tree, return the parent node. If the key is already in the
        tree, return the node that contains it. This function returns None if the
        given node is equal to None.
        """
        if not node or key == node.key:
            return node
        elif key < node.key:
            if node.left:
                return self.binaryHelper(key, node.left)
            else:
                return node  # Return the parent node
        elif key > node.key:
            if node.right:
                return self.binaryHelper(key, node.right)
            else:
                return node

    # from https://stackoverflow.com/questions/34012886/print-binary-tree-level-by-level-in-python
    def printTree(self, node, level=0):
        if node is not None:
            self.printTree(node.left, level + 1)
            print(' ' * 4 * level + '->', node.value)
            self.printTree(node.right, level + 1)

class TreeNode(object):
    """Tree Node object.
    A tree node is an entry within a binary tree that holds a key value pair.
    Member Variables:
      _key the key that this node contains. TreeNodes are searched by key.
      _value the value that corresponds to the stored key.
      parent the parent of this node.
      left the left child node which has a key less than this node.
      right the right child node which has a key less than this node.
    """

    def __init__(self, key, value, parent=None, left=None, right=None):
        """Initialize a Tree Node object given certain values."""

        self._key = key
        self._value = value
        self.parent = parent
        self.left = left
        self.right = right

    @property
    def key(self):
        """Return the key stored by this tree node."""
        return self._key

    @property
    def value(self):
        """Return the value stored by this tree node."""
        return self._value

    @property
    def isLeftChild(self):
        """Return whether this node is a left child."""
        if self.parent:
            return (self.parent).left is self

    @property
    def isRightChild(self):
        """Return whether this node is a right child."""
        if self.parent:
            return (self.parent).right is self
