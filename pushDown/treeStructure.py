import collections


class Node:
    def __init__(self, guest, _index):
        self.guest = guest
        self.left = self.right = None
        self._index = _index

    def get_guest(self):
        if self.guest is None:
            raise Exception("Guest of node not available")
        return self.guest

    def get_index(self):
        return self._index

    def __str__(self):
        return str(self.guest)


class Item:
    def __init__(self, id):
        self.id = id

    def __str__(self):
        return str(self.id)


class CompleteTree:

    def __init__(self):
        self.root = None
        self.index2item_id = dict()
        self.bit_string2index = dict()

    def insert(self, newItem):
        """Insert a new value in the tree. Takes one argument (the Item) """
        parent = self.root

        # check if key=item_id already in tree
        if newItem.id in self.index2item_id:
            return
        if parent is None:

            # root is null, make this the new root, index == 0, done
            self.root = Node(newItem, 0)
            self.index2item_id[newItem.id] = 0
            self.bit_string2index[0] = collections.deque()
            return
        else:
            currIndex = len(self.index2item_id)
            parentIndex, bit = -99, -99
            if (currIndex % 2 == 0):  # right child
                parentIndex = (currIndex - 2) / 2
                bit = 1
            else:
                parentIndex = (currIndex - 1) / 2  # left child
                bit = 0
            newBitString = self.bit_string2index[parentIndex].copy()
            pathToConsume = newBitString.copy()
            parent = self.getParentOfInsertingNode(pathToConsume)
            newNode = Node(newItem, currIndex)
            if bit == 1:
                parent.right = newNode
            else:
                parent.left = newNode
            newBitString.append(bit)
            self.bit_string2index[currIndex] = newBitString
            self.index2item_id[newItem.id] = currIndex

    def getParentOfInsertingNode(self, path):
        currNode = self.root

        while path:
            if path.popleft() == 1:
                currNode = currNode.right
            else:
                currNode = currNode.left
        return currNode

    def get_index_of_item(self, item_id):
        if item_id in self.index2item_id:
            return self.index2item_id[item_id]
        else:
            return -99

    def get_depth_of_item(self, item_id):
        index = self.get_index_of_item(item_id)
        if index == 0:
            return 0
        else:
            # 0
            # 1 2
            # 3 4 5 6
            nodeDepth = 1
            # pow(2, nodeDepth)-1 gets leftmost node of each level
            while (pow(2, nodeDepth) - 1) <= index:
                nodeDepth += 1

            # upper level of index reached, hence substract one for desired level
            return nodeDepth - 1

    def getNodesInOrder(self):
        buf = collections.deque()
        root = self.root
        output = []
        final = []
        if not root:
            print("No root")
        else:
            buf.append(root)
            count, nextCount = 1, 0
            while count:
                node = buf.popleft()
                if node:
                    output.append(node)
                    count -= 1
                    for n in (node.left, node.right):
                        if n:
                            buf.append(n)
                            nextCount += 1
                        else:
                            buf.append(None)
                if not count:
                    final.append(output)
                    count, nextCount = nextCount, 0
        return output

    def fillTreeForSource(self, sourceId, demandSequence):
        """
        fills the tree with requests from a given source
        the items in the tree are the destinations of the request
        """

        # demand-format is (source, destination, demand, timestamp)
        requests_from_source = collections.deque()
        destinations = collections.deque()
        for x in demandSequence:
            if x[0] == sourceId:
                requests_from_source.append(x)
                destinations.append(x[1])
                #print("Appended " + str(x[1]))
        for x in destinations:
            if x not in self.index2item_id:
                self.insert(Item(x))

def print_tree(root):
    print("Current Treestructure:")
    buf = collections.deque()
    output = []
    if not root:
        print("No root")
    else:
        buf.append(root)
        count, nextCount = 1, 0
        while count:
            node = buf.popleft()
            if node:
                output.append(node.guest)
                count -= 1
                for n in (node.left, node.right):
                    if n:
                        buf.append(n)
                        nextCount += 1
                    else:
                        buf.append(None)
            if not count:
                print(*output)
                output = []
                count, nextCount = nextCount, 0
