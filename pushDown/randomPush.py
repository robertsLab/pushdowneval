from pushDown.treeStructure import *
import random
from math import log2

class randomPush():

    def __init__(self, tree):
        self.debug = False
        self.tree = tree

        # for testing
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nRandom Push-Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)
        if index == 0:
             return self.tree.root
        if index == -99:
             print("No Item for " + str(item_id) + " found")
        else:
            try:
                return self.pull_up_item(item_id)
            except Exception as e:
                print(e.args)

    def pull_up_item(self, item_id):
        """
        pushes root down one step down the path to random_item at u.dep and pulls u up to root 
        """
        depth = self.tree.get_depth_of_item(item_id)
        self.access_cost += depth
        path = self.generate_random_path_of_size(depth)      # generate a path for random-walk to push down v
        self.pull_up_w(path)        # pull up w at root
        if self.debug:
            print("pulled up w")
        path = self.tree.bit_string2index[self.tree.get_index_of_item(item_id)].copy()   # take path to get to the accessed item
        if path:
            self.pushRootAlongPath(path)
        if self.debug:
            print_tree(self.tree.root)
        return self.tree.root
    
    def generate_random_path_of_size(self, depth):
        k = depth
        path = collections.deque()
        while not k == 0:
            random_step = random.randint(0, 1)
            path.append(random_step)
            k -= 1
        return path

    def pull_up_w(self, path):
        """ pulls up to root the chosen random item and consequently pushes Root one step along path  """
        parent = self.tree.root
        child = None
        if path:
            child = self.get_next_child(parent, path)
            if child is None:
                raise Exception("Generated a path for pushing root, but root has no children.")
        k = len(path)
        self.swap_defined_sequence_only_up(parent, child, path, k)

    def pushRootAlongPath(self, path):
        """ pushes Root along path and finally pulls up to root the accessed item """
        parent = self.tree.root
        child = None
        if path:
            child = self.get_next_child(parent, path)
            if child is None:
                raise Exception("Generated a path for pushing root, but root has no children.")
        k = len(path)
        self.swap_defined_sequence(parent, child, path, k)

    def swap_defined_sequence(self, parent, child, path, k):

        # swap the root down to the accessed level -> push-down-part
        self.swap(parent, child)
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            if follow_child is None:
                k -= 1
            else:
                self.swap_defined_sequence(follow_parent, follow_child, path, k-1)
        
        """swap the accessed item up to the root. Last original swap not to be reversed -> Pull-up-part"""
        if k > 0:
            self.swap(parent, child)

    def swap_defined_sequence_only_up(self, parent, child, path, k):
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            if follow_child is None:
                k -= 1
            else:
                self.swap_defined_sequence_only_up(follow_parent, follow_child, path, k - 1)

        """swap the accessed item up to the root. Last original swap not to be reversed -> Pull-up-part"""
        self.swap(parent, child)

    def swap(self, one, two):
        if self.debug:
            print("Swapping " + str(one.guest.id) + " with " + str(two.guest.id))
        temp = one.get_guest()
        one.guest = two.guest
        two.guest = temp
        self.tree.index2item_id[one.guest.id] = one.get_index()
        self.tree.index2item_id[two.guest.id] = two.get_index()
        self.swaps += 1

    # gets the next child according to path
    # returns None if no left or right children available
    def get_next_child(self, parent, path):
        child = None
        if path.popleft() == 1 and parent.right:
            child = parent.right
        elif parent.left:                                  # if right child not available, check if there's a left child
            child = parent.left
        return child