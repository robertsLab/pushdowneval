import unittest
from pushDown.treeStructure import *
from pushDown.randomPush import *
from pushDown.minMaxPush import *
from pushDown.rotatePush import *

#inherit from testcase
class TestPushDown(unittest.TestCase):
    
    def setUp(self):
        a,b,c,d,e,x,y,z = Item(10), Item(11), Item(12), Item(13), Item(14), Item(15), Item(16), Item(17)
        arr = [a,b,c,d,e,x,y,z]
        self.ct = CompleteTree()
        for x in arr:
            self.ct.insert(x)
        self.p = randomPush(self.ct)
        self.p2 = minPush(self.ct)
        self.p3 = rotatePush(self.ct)

    def testGetIndexOfRootNode_shouldReturnIndex0(self):
       self.assertEqual(self.ct.get_index_of_item(10), 0)
       
    def testget_index_of_item12_shouldReturnIndex2(self):
        self.assertEqual(self.ct.get_index_of_item(12), 2)

    def testFindDepthOfItems(self):
        a = self.ct.get_depth_of_item(10)
        b = self.ct.get_depth_of_item(12)
        c = self.ct.get_depth_of_item(14)
        arr = [a, b, c]
        res = [0, 1, 2]
        self.assertEqual(arr, res)

    def testCorrectBitString0(self):
        path = self.ct.bit_string2index[0]
        shouldBe = collections.deque()
        self.assertEqual(shouldBe, path)
    
    def testCorrectBitString4(self):
        path = self.ct.bit_string2index[4]
        shouldBe = collections.deque([0,1])
        self.assertEqual(shouldBe, path)

    def testCorrectBitString2(self):
        path = self.ct.bit_string2index[5]
        shouldBe = collections.deque([1,0])
        self.assertEqual(shouldBe, path)

    def testGetParent(self):
        parent = self.ct.getParentOfInsertingNode(self.ct.bit_string2index[7])
        self.assertEqual(parent.guest.id, 17)

    def testRandomMultiplePushesLastAccess16_16atRoot(self):
        self.p.access(12)
        self.p.access(16)
        self.p.access(14)
        self.p.access(11)
        self.p.access(10)
        self.p.access(16)
        self.assertEqual(self.ct.root.get_guest().id, 16)
   
    def testMinMultiplePushesLastAccess16_16atRoot(self):
        self.p2.access(12)
        self.p2.access(16)
        self.p2.access(14)
        self.p2.access(11)
        self.p2.access(10)
        self.p2.access(16)
        self.assertEqual(self.ct.root.get_guest().id, 16)

    """
    next 2 tests assert that items 12, 15, 14, 13 are on depth 2
    this has to be True since other 3 items have been pulled up after they have been accessed
    """
    def testMinLeastRU(self):
        self.p2.access(12)
        self.p2.access(15)
        self.p2.access(14)
        self.p2.access(13)
        self.p2.access(11)
        self.p2.access(10)
        self.p2.access(16)
        depth = 2
        lastIndex = pow(2, depth+1)-1                   #last index of depth 2
        index1 = self.ct.get_index_of_item(13)
        index2 = self.ct.get_index_of_item(14)
        index3 = self.ct.get_index_of_item(15)
        index4 = self.ct.get_index_of_item(12)
        list = [index1, index2, index3, index4]
        maxInd = max(list)
        self.assertFalse(maxInd > lastIndex)

    def testMinLeastRU2(self):
        self.p2.access(12)
        self.p2.access(15)
        self.p2.access(14)
        self.p2.access(13)
        self.p2.access(11)
        self.p2.access(10)
        self.p2.access(16)
        depth = 2
        firstIndex = pow(2, depth)-1                    #first index of depth 2
        index1 = self.ct.get_index_of_item(13)
        index2 = self.ct.get_index_of_item(14)
        index3 = self.ct.get_index_of_item(15)
        index4 = self.ct.get_index_of_item(12)
        list = [index1, index2, index3, index4]
        minInd = min(list)
        self.assertFalse(minInd < firstIndex)

    def testRotateMultiplePushesLastAccess16_16atRoot(self):
        self.p3.access(12)
        self.p3.access(16)
        self.p3.access(14)
        self.p3.access(11)
        self.p3.access(10)
        self.p3.access(16)
        self.assertEqual(self.ct.root.get_guest().id, 16)

    def tearDown(self):
        root = None
        self.ct = None
        self.p = None


if __name__ == '__main__':
    unittest.main()


