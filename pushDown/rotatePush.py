from pushDown.treeStructure import *

class rotatePush():
    
    def __init__(self, tree):
        self.tree = tree
        self.direction2index = dict()
        inv_map = {v: k for k, v in self.tree.index2item_id.items()}
        for x in inv_map:
            self.direction2index[x] = 0        # initialize each nodeIndex to push to the left (=0) first

        # for testing
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0
        self.debug = False

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nRotate Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)
        if index == 0:
             return self.tree.root
        if index == -99:
             print("No Item for " + str(item_id) + " found")
        else:
            try:
                return self.pull_up_item(item_id)
            except Exception as e:
                print(e.args)

    def pull_up_item(self, item_id):
        depth = self.tree.get_depth_of_item(item_id)
        self.access_cost += depth

        # pull up w at root
        self.alternate_pull_up_from_depth(depth)
        if self.debug:
            print("Pulled up w from depth " + str(depth))
        if not self.tree.root.get_guest().id == item_id:

            # u has not been pulled up "accidentally". Pull up item u
            searched_index = self.tree.index2item_id[item_id]
            path = self.tree.bit_string2index[searched_index].copy()
            child = self.get_next_child(self.tree.root, path)
            self.swap_defined_sequence(self.tree.root, child, path, len(path))
        if self.debug:
            print_tree(self.tree.root)
        return self.tree.root

    def alternate_pull_up_from_depth(self, depth):
        """
        pull up selected item at depth according to the defined direction (at first, left children are selected)
        -> once a node hosts the item, change the direction of the node
        """
        child = None
        if self.direction2index[0] == 0 or not self.tree.root.right:  # second case=when tree has 2 nodes only
            child = self.tree.root.left
        else: 
            child = self.tree.root.right
        self.swap_alternatively(self.tree.root, child, depth)

    def swap_alternatively(self, parent, child, k):

        # switch direction for current node
        if self.direction2index[parent.get_index()] == 0:
            self.direction2index[parent.get_index()] = 1
        else:
            self.direction2index[parent.get_index()] = 0

        # check if further steps left. Stop at k=1 because for depth k, k-1 swaps required
        if k > 1:
            follow_child = None
            follow_parent = child
            if self.direction2index[child.get_index()] == 0:
                follow_child = child.left
            else: 
                follow_child = child.right
            if follow_child is None:        # end of tree, item is at level lower but on left side
                self.swap(parent, child)
                return
            self.swap_alternatively(follow_parent, follow_child, k-1)

        # swap the last item at depth(u) up to the root
        self.swap(parent, child)

      
    
    def swap_defined_sequence(self, parent, child, path, k):
        self.swap(parent, child)
        if path:
            follow_parent = child
            follow_child = self.get_next_child(follow_parent, path)
            self.swap_defined_sequence(follow_parent, follow_child, path, k-1)
       
        # swap the accessed item up to the root. Last original swap not to be reversed
        if k > 0:
            self.swap(parent, child)
    
    def get_next_child(self, parent, path):
        child = None
        if path.popleft() == 1 and parent.right:
            child = parent.right
        elif parent.left:                                  # if right child not available, check if there's a left child
            child = parent.left
        
        # no left or right children available --> return None
        return child

    def swap(self, one, two):
        if self.debug:
            print("swapping " + str(one.guest) + " WITH " +str(two.guest))
        temp = one.get_guest()
        one.guest = two.guest
        two.guest = temp
        self.tree.index2item_id[one.guest.id] = one.get_index()
        self.tree.index2item_id[two.guest.id] = two.get_index()
        self.swaps += 1
