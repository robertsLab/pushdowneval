from pushDown.treeStructure import *

class static_offline():
    def __init__(self, tree):
        self.tree = tree
        self.debug = False
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    # for testing
    def reset(self):
        self.swaps = 0
        self.adjust_cost = 0
        self.access_cost = 0

    def access(self, item_id):
        if self.debug:
            print("\nstatic_offline Access " + str(item_id))
        index = self.tree.get_index_of_item(item_id)

        if index == 0:
            return self.tree.root
        if index == -99:
            print("No Item for " + str(item_id) + " found")
        else:
            depth_of_u = self.tree.get_depth_of_item(item_id)
            self.access_cost += depth_of_u
            index_of_u = self.tree.get_index_of_item(item_id)
            path_to_u = self.tree.bit_string2index[index_of_u].copy()
            parent = self.tree.root
            child = self.get_next_child(parent, path_to_u)
            self.route(parent, child, path_to_u, item_id)

    # route function, useful for adding some execution time to the algo
    def route(self, parent, child, path_to_u, accessed_item):
        if not child:
            return
        if not path_to_u:
            follow_child = None
        else:
            follow_child = self.get_next_child(child, path_to_u)
        follow_parent = child
        self.route(follow_parent, follow_child, path_to_u, accessed_item)

    def get_next_child(self, parent, path):
        child = None
        if path.popleft() == 1 and parent.right:
            child = parent.right
        else:                   # if right child not available, check if there's a left child
            child = parent.left

        if child is None:
            raise Exception('No child_node for parent ', str(parent.get_guest()), ' found!')

        # no left or right children available --> return None
        return child