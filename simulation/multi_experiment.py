from simulation.bar_plotter import plot_bar, plot_bar_nosplay
from simulation.data_handler import save_seq_dict, get_calgary_original
from simulation.experiment import *
import simulation.artificial_experiment as ae


# 1) paper experiments
def run_fix_net_calgary():
    """run experiment on all vocabulary-datasets, with fixed request and net size"""
    algos = ["random", "rotate", "static_offline", "half_opt", "max", "splay", "static_oblivious"]
    size_dataset = 0
    n_hosts = 0
    num_simulations = 5
    res_dict = dict()  # dict for storing all results of the exp
    means = False       # True: result is means of total costs, False: result is all costs to serve each req
    num_datasets = num_simulations

    seq_dict = dict()
    while num_simulations:
        sequences_array = []

        # fetching triples
        sequences_array.append(get_calgary_original(sample=num_datasets-num_simulations))
        saving_sequence = []
        for request in sequences_array[0]:      # there will be one sequence only, for each simulation
            saving_sequence.append(request)
        seq_dict["book"+str(num_datasets-num_simulations+1)] = dict()
        seq_dict["book"+str(num_datasets-num_simulations+1)]["trace variants"] = dict()
        seq_dict["book" + str(num_datasets - num_simulations + 1)]["trace variants"]["original-trace"] = str(saving_sequence)
        unique_seq = []
        for i in sequences_array:   # it will be one sequence only for each simulation
            print("Seq counts nodes= " + str(len(ae.count_nodes(i))))
            print("Seq counts req= " + str(len(i)))
            #print(i)
            unique_seq = i
        sorted = ae.sort_nodes(unique_seq)
        #print(unique_seq)
        tree_nodes = []
        for x in sorted:
            tree_nodes.append(x[0])
        random.shuffle(tree_nodes)      # random order of nodes
        exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req = False,
                         means=means, n_hosts=n_hosts)
        entropy_set = ae.calc_entropy(sequences_array)
        temporal_p_set = ae.calc_temporal_locality(sequences_array, artificial=True)
        entropy = 0
        for e in entropy_set:
            entropy += e
        entropy /= len(entropy_set)  # take mean of entropies
        temp_p = 0
        for p in temporal_p_set:
            temp_p += p
        temp_p /= len(temporal_p_set)

        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=True)

            # prepare for csv-write
            del res[0]            # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])     # insert name of algo
            res.append(0)
            res.append(round(entropy,2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]]["book"+str(num_datasets-num_simulations+1)] = res      # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()         # create dict for this algo
                res_dict[algos[i]]["book"+str(num_datasets-num_simulations+1)] = res
        #ew.write_dist_single_for_multi(sequences_array, "real_sets", simulation_id=num_datasets-num_simulations)
        num_simulations -= 1
    save_seq_dict("q1", seq_dict)
    # plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q1", stand_dev=True, use_median=False,
    #                  acc_only=False, adj_only=False)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q1", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=False)
    # plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q1", stand_dev=True, use_median=False,
    #                  acc_only=False, adj_only=True)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q1", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=True)
    plot_bar(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q1", stand_dev=False, use_median=False,acc_only=True)