from pushDown.half_move_opt import half_move_opt
from pushDown.static_offline import static_offline
from pushDown.rotatePush import *
from pushDown.randomPush import *
from pushDown.minMaxPush import *
from pushDown.splayTree import *
from pushDown.treeStructure import *
import simulation.data_generator as dg
import time

class simulateAlgo():
    def __init__(self):
        self.demandSequence = None
        self.tree_nodes = None
        self.ct = None      # complete tree for push-down algos
        self.bt = None      # binary tree for splay-algo
        self.source = None
        self.source_counter = None
        self.num_sources = 0
        self.num_requests = 0
        self.real_serving_limit = 100000        # hard-coded limit for real datasets
        self.most_freq_experiment = False

    def fix_data(self, demand_sequence, tree_nodes):
        self.demandSequence = demand_sequence
        self.tree_nodes = tree_nodes


    def set_up_artificial(self):
        """
            For artificial distributions, fill a tree with all data of the demandSequence, same source
            """
        self.ct = CompleteTree()
        self.bt = SplayTree()
        destinations = []
        if not self.tree_nodes:
            for x in self.demandSequence:
                if x not in destinations:
                    destinations.append(x)
            destinations = random.sample(destinations, len(destinations))  # shuffle the order in which the nodes are put in tree
        else:
            for x in self.tree_nodes:       # these are already shuffled
                if x not in destinations:
                    destinations.append(x)
        for i in range(0, len(destinations)):
            self.ct.insert(Item(destinations[i]))
            self.bt.insert(destinations[i], Item(destinations[i]))
        return self.demandSequence

    def set_up_artificial_opt(self):            # for static, optimal tree
        self.ct = CompleteTree()
        self.bt = SplayTree()
        sorted_set = sort_nodes(self.demandSequence)
        listed_set = []
        for x in sorted_set:
            listed_set.append(x[0])
        listed_set.reverse()
        for x in listed_set:
            self.ct.insert(Item(x))
            self.bt.insert(x, Item(x))
        return self.demandSequence

    def execute_artificial(self, algoType, destinations):
        """
            executes the desired access-algorithm on the tree
            all registered destinations got to be in the demand-sequence
            return results for serving the whole sequence
            """
        self.num_requests = 0
        chosenAlgo = None
        if algoType == "min":
           chosenAlgo = minPush(self.ct)
        elif algoType == "random":
            chosenAlgo = randomPush(self.ct)
        elif algoType == "rotate":
            chosenAlgo = rotatePush(self.ct)
        elif algoType == "static_offline" or algoType == "static_oblivious":
            chosenAlgo = static_offline(self.ct)
        elif algoType == "half_opt":
            chosenAlgo = half_move_opt(self.ct)
        elif algoType == "max":
            chosenAlgo = maxPush(self.ct)
        elif algoType != "splay":
            raise Exception("Invalid input - The algorithm options are <min>, <random>, <half>, "
                            "<max>, <splay>, <static_offline>, <static_oblivious> and <rotate>")
        start = time.time()
        for x in self.demandSequence:
            if x in destinations:
                if algoType == "splay":
                    self.bt.find(x)
                else:
                    chosenAlgo.access(x)
                self.num_requests += 1
            else:
                raise Exception("Destination not registered")
        end = time.time()
        if algoType == "splay":
            return [(end - start) * 1000, self.bt.swaps, self.bt.adjust_cost, self.bt.access_cost, self.num_requests]
        else:
            return [(end - start) * 1000, chosenAlgo.swaps, chosenAlgo.adjust_cost, chosenAlgo.access_cost,
                    self.num_requests]

    def execute_artificial_return_single(self, algoType, destinations):
        """
            executes the desired access-algorithm on the tree
            all registered destinations got to be in the demand-sequence
            returns all results
            """
        self.num_requests = 0
        chosenAlgo = None
        if algoType == "min":
           chosenAlgo = minPush(self.ct)
        elif algoType == "random":
            chosenAlgo = randomPush(self.ct)
        elif algoType == "rotate":
            chosenAlgo = rotatePush(self.ct)
            # print("Items in tree: " + str(len(self.ct.index2item_id)))
            # print("Number of items in sequence: " + str(len(dg.sorted_nodes_occur(self.demandSequence)))
            #       + ", len of seq : "+ str(len(self.demandSequence)))
        elif algoType == "static_offline" or algoType == "static_oblivious":
            chosenAlgo = static_offline(self.ct)
            print("Oblivious: Items in tree: " + str(len(self.ct.index2item_id)))
            print("Number of items in sequence: " + str(len(dg.sorted_nodes_occur(self.demandSequence)))
                  + ", len of seq : " + str(len(self.demandSequence)))
        elif algoType == "half_opt":
            chosenAlgo = half_move_opt(self.ct)
        elif algoType == "max":
            chosenAlgo = maxPush(self.ct)
        elif algoType != "splay":
            raise Exception("Invalid input - The algorithm options are <min>, <random>, <half>, "
                            "<max>, <splay>, <static_offline> and <rotate>")
        costs = dict()
        costs['access'] = []
        costs['adjust'] = []
        costs['swaps'] = []
        start = time.time()
        for x in self.demandSequence:
            if x in destinations:
                if algoType == "splay":
                    self.bt.find(x)
                    costs['access'].append(self.bt.access_cost)
                    costs['adjust'].append(self.bt.adjust_cost)
                    costs['swaps'].append(self.bt.swaps)
                    self.bt.reset_costs()
                else:
                    chosenAlgo.access(x)
                    costs['access'].append(chosenAlgo.access_cost)
                    costs['adjust'].append(chosenAlgo.adjust_cost)
                    costs['swaps'].append(chosenAlgo.swaps)
                    chosenAlgo.reset()
                self.num_requests += 1

            else:
                raise Exception("Destination not registered")
        end = time.time()
        return [(end - start) * 1000, costs['swaps'], costs['adjust'], costs['access'],
                    self.num_requests]

    # as above, but:
    # keep track of and return the cost for serving each request
    def execute_artificial_per_req(self, algoType, destinations):
        self.num_requests = 0
        chosenAlgo = None
        if algoType == "min":
           chosenAlgo = minPush(self.ct)
        elif algoType == "max":
            chosenAlgo = maxPush(self.ct)
        elif algoType == "random":
            chosenAlgo = randomPush(self.ct)
        elif algoType == "rotate":
            chosenAlgo = rotatePush(self.ct)
        elif algoType == "static_offline" or algoType == "static_oblivious":
            chosenAlgo = static_offline(self.ct)
        elif algoType == "half_opt":
            chosenAlgo = half_move_opt(self.ct)
        elif algoType == "splay":
            self.bt.reset()
        else:
            raise Exception("Invalid input - The algorithm options are <min>, <random>, <half>, "
                            "<max>, <splay>, <static_offline> and <rotate>")
        per_req_dict = dict()
        for x in self.demandSequence:
            serving_req_start = time.time()             # save the result for the actual request
            if x in destinations:
                if chosenAlgo == "splay":
                    self.bt.find(x)
                else:
                    chosenAlgo.access(x)
                serving_req_end = time.time()

                # for each req_num, save a list for the result
                per_req_dict[self.num_requests] = [(serving_req_end-serving_req_start)*1000,
                                                   chosenAlgo.swaps, chosenAlgo.adjust_cost, chosenAlgo.access_cost]
                self.num_requests += 1
                chosenAlgo.reset()
            else:
                raise Exception("Destination not registered")
        print("Served requests = " + str(self.num_requests))
        return per_req_dict

    def set_up_environment(self):
        """
        Generates a tree filled with the DB-data, putting both values of pair [src,dst] in the tree.
        As if src,dst would not be distinguished.
        """
        self.ct = CompleteTree()
        self.bt = SplayTree()
        destinations = []
        for x in self.demandSequence:
            if x[0] not in destinations:
                destinations.append(x[0])
            if x[1] not in destinations:
                destinations.append(x[1])
        res = random.sample(destinations, len(destinations))        # shuffle the order in which the nodes are put in tree
        for x in res:
            self.ct.insert(Item(x))
            self.bt.insert(x, Item(x))

    def set_up_opt(self, most_freq):
        """
        For Static, optimal tree
        Generates a tree filled with the DB-data, putting both values of pair [src,dst] in the tree.
        As if src,dst would not be distinguished.
        """
        self.ct = CompleteTree()
        self.bt = SplayTree()
        all_req = []
        if most_freq:
            self.most_freq_experiment = True
            self.source_counter = collections.defaultdict(int)

            # for every source, count how many requests originate from it
            # a request x looks like (source, destination, demand, timestamp)
            for x in self.demandSequence:
                if x[0] in self.source_counter:
                    self.source_counter[x[0]] += 1  # increment existing source
                else:
                    self.source_counter[x[0]] = 0  # put new source in dict
            most_freq_source = max(self.source_counter, key=self.source_counter.get)
            print("\nOPTIMAL Filling a tree with destinations of most frequent source in sequence (id = "
                  + str(most_freq_source) + ")")
            self.source = most_freq_source
            for x in self.demandSequence:
                if x[0] == self.source:
                    all_req.append(x[1])
        else:
            for x in self.demandSequence:
                all_req.append(x[0])
                all_req.append(x[1])
        sorted_set = sort_nodes(all_req)
        listed_set = []
        for x in sorted_set:
            listed_set.append(x[0])
        listed_set.reverse()
        for x in listed_set:
            self.ct.insert(Item(x))
            self.bt.insert(x, Item(x))

    def set_up_environment_most_freq(self):
        """Generates a tree filled with the DB-data.
            Determines the most frequent source for a sequence and fills the tree with its destinations
            """
        self.most_freq_experiment = True
        self.source_counter = collections.defaultdict(int)

        # for every source, count how many requests originate from it
        # a request x looks like (source, destination, demand, timestamp)
        for x in self.demandSequence:
            if x[0] in self.source_counter:
                self.source_counter[x[0]] += 1  # increment existing source
            else:
                self.source_counter[x[0]] = 0  # put new source in dict
        most_freq_source = max(self.source_counter, key=self.source_counter.get)
        print("\nFilling a tree with destinations of most frequent source in sequence (id = "
              + str(most_freq_source) + ")")
        self.ct = CompleteTree()
        self.ct.fillTreeForSource(most_freq_source, self.demandSequence)
        self.source = most_freq_source
        self.bt = SplayTree()
        for req in self.demandSequence:
            if req[0] == most_freq_source:
                self.bt.insert(req[1], Item(req[1]))


    def execute_real_return_single(self, algoType):
        """
            executes the desired access-algorithm on the tree
            returns all results
            setUpEnvironment must be called first.
            """
        self.num_requests = 0
        chosenAlgo = None
        if algoType == "min":
            chosenAlgo = minPush(self.ct)
        elif algoType == "random":
            chosenAlgo = randomPush(self.ct)
        elif algoType == "rotate":
            chosenAlgo = rotatePush(self.ct)
        elif algoType == "static_offline" or algoType == "static_oblivious":
            chosenAlgo = static_offline(self.ct)
        elif algoType == "half_opt":
            chosenAlgo = half_move_opt(self.ct)
        elif algoType == "max":
            chosenAlgo = maxPush(self.ct)
        elif algoType != "splay":
            raise Exception("Invalid input - The algorithm options are <min>, <random>, <half>, "
                            "<max>, <splay>, <static_offline> and <rotate>")
        flat_seq=[]
        if self.most_freq_experiment:
            if not self.source and self.source != 0:       # used if we fill tree with destinations of one src only
                raise Exception("Source not initialized. setUpEnvironment must be called first")
            temp_seq = self.demandSequence.copy()
            for i in range(len(temp_seq)):
                if temp_seq[i][0] == self.source:
                    flat_seq.append(temp_seq[i][1])
        else:
            for i in range(len(self.demandSequence)):
                temp = self.demandSequence[i][:2]  # remove timestamp and demand
                self.demandSequence[i] = temp
            flat_seq = [item for sublist in self.demandSequence for item in sublist]  # src, dst are flattened
        costs = dict()
        costs['access'] = []
        costs['adjust'] = []
        costs['swaps'] = []
        start = time.time()
        print(flat_seq)
        for x in flat_seq:
            if algoType == "splay":
                self.bt.find(x)
                costs['access'].append(self.bt.access_cost)
                costs['adjust'].append(self.bt.adjust_cost)
                costs['swaps'].append(self.bt.swaps)
                self.bt.reset_costs()
            else:
                chosenAlgo.access(x)
                costs['access'].append(chosenAlgo.access_cost)
                costs['adjust'].append(chosenAlgo.adjust_cost)
                costs['swaps'].append(chosenAlgo.swaps)
                chosenAlgo.reset()
            self.num_requests += 1
            if self.num_requests >= self.real_serving_limit:
                #print("Surpassed limit!")
                break
        end = time.time()
        return [(end - start) * 1000, costs['swaps'], costs['adjust'], costs['access'],
                self.num_requests]

    # as above, but:
    # keep track of and return the cost for serving each request
    def execute_real_per_req(self, algoType):
        self.num_requests = 0
        chosenAlgo = None
        if algoType == "min":
            chosenAlgo = minPush(self.ct)
        elif algoType == "random":
            chosenAlgo = randomPush(self.ct)
        elif algoType == "rotate":
            chosenAlgo = rotatePush(self.ct)
        elif algoType == "static_offline" or algoType == "static_oblivious":
            chosenAlgo = static_offline(self.ct)
        elif algoType == "half_opt":
            chosenAlgo = half_move_opt(self.ct)
        elif algoType == "max":
            chosenAlgo = maxPush(self.ct)
        elif algoType == "splay":
            self.bt.reset()
        else:
            raise Exception("Invalid input - The algorithm options are <min>, <random>, <half>, "
                            "<max>, <splay>, <static_offline> and <rotate>")
        per_req_dict = dict()
        for x in self.demandSequence:
            serving_req_start = time.time()  # save the result for the actual request
            if x[0] == self.source:  # x[0] == source
                chosenAlgo.access(x[1])
                serving_req_end = time.time()

                # for each req_num, save a list for the result
                per_req_dict[self.num_requests] = [(serving_req_end - serving_req_start) * 1000,
                                                   chosenAlgo.swaps, chosenAlgo.adjust_cost, chosenAlgo.access_cost]
                self.num_requests += 1
                chosenAlgo.reset()
        print("Served requests = " + str(self.num_requests))
        return per_req_dict

def sort_nodes(set):
    """returns the set of distinct nodes (hosts) in the sequence, sorted by their occurrence (max to min)"""
    counter = dict()
    for item in set:
        if item in counter:
            counter[item] += 1
        else:
            counter[item] = 0
    new_set = sorted(counter.items(), key=lambda x: x[1])
    return new_set
