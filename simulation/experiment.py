import math
import logging

from simulateAlgo import *


class experiment():
    def __init__(self, tree_nodes, sequences_array, artificial, incr_req, means, n_hosts):
        self.num_simulations = len(sequences_array)
        self.sequences_array = sequences_array
        self.tree_nodes = tree_nodes
        self.artificial = artificial
        self.incr_req = incr_req
        self.means = means
        self.n_hosts = n_hosts

        if self.incr_req:
            self.per_sequence_dict = dict()
            for i in range(0, self.num_simulations):
                self.per_sequence_dict[i] = dict()
        else:
            if self.means:
                self.cumulative_result = dict()
                self.cumulative_result['mean_exec_time'] = 0
                self.cumulative_result['mean_swaps'] = 0
                self.cumulative_result['mean_adjust'] = 0
                self.cumulative_result['mean_access'] = 0
                self.cumulative_result['num_requests'] = 0
            else:
                self.variant_result = dict()
                self.variant_result['exec_time'] = []
                self.variant_result['swaps'] = []
                self.variant_result['adjust'] = []
                self.variant_result['access'] = []
                self.variant_result['num_requests'] = []
        self.algo_type = None

    def reset(self):
        if self.incr_req:
            self.per_sequence_dict = dict()
        else:
            if self.means:
                self.cumulative_result['mean_exec_time'] = 0
                self.cumulative_result['mean_swaps'] = 0
                self.cumulative_result['mean_adjust'] = 0
                self.cumulative_result['mean_access'] = 0
                self.cumulative_result['num_requests'] = 0
            else:
                self.variant_result['exec_time'] = []
                self.variant_result['swaps'] = []
                self.variant_result['adjust'] = []
                self.variant_result['access'] = []
                self.variant_result['num_requests'] = []

    def run(self, algo_type, single_results):
        """
        Run experiment for the chosen algorithm on the given demand-sequences
        if incr_req: return an array with cost-means for serving each request (in each sequence)
        if incr_net: return the cost-means for serving the sequences
        """
        self.reset()
        self.algo_type = algo_type
        sim = simulateAlgo()
        print('Starting <', self.num_simulations, '> simulations of <', self.algo_type, '> algorithm')
        start = time.time()             # record time to run i simulations of algorithm

        # do i simulations
        for i in range(0, len(self.sequences_array)):
            print("Working on " + str(len(self.sequences_array[i])) + " requests (2x for db-data)")
            mod_tree_nodes = False
            if not self.tree_nodes:
                mod_tree_nodes = True
                self.tree_nodes = correct_n_hosts(sequence=self.sequences_array[i].copy(), n_hosts=self.n_hosts)
            sim.fix_data(demand_sequence=self.sequences_array[i], tree_nodes=self.tree_nodes)
            if mod_tree_nodes:
                self.tree_nodes = []
            if self.artificial:

                # for each sequence, set up new tree and data (nodes)
                if algo_type == "static_offline":
                    items = sim.set_up_artificial_opt()
                else:
                    items = sim.set_up_artificial()

                # execute one algorithm for the whole sequence
                if self.incr_req:
                    result = sim.execute_artificial_per_req(self.algo_type, items)
                else:
                    if single_results:  # return the single costs to serve each request, as a dict of lists
                        result = sim.execute_artificial_return_single(self.algo_type, items)
                    else:           # return the total cost to serve the whole sequence only
                        result = sim.execute_artificial(self.algo_type, items)
            else:       # experiment with real data
                raise Exception("Not implemented for the paper-experiments!")

            # result will be list = [execution time(millis), swaps, adjustment-cost, acces_cost, served requests]
            # in case of incr_req: result = dict() where key = request#,
            # and value = [time to serve req in millis, swaps, adjust_cost, access_cost]
            if self.incr_req:
                self.per_sequence_dict[i] = result      # each sequence gets a place in dict
            else:       # incr_net
                if self.means:      # accumulate resulting values
                    self.cumulative_result['mean_exec_time'] += result[0]
                    self.cumulative_result['mean_swaps'] += result[1]
                    self.cumulative_result['mean_adjust'] += result[2]
                    self.cumulative_result['mean_access'] += result[3]
                    self.cumulative_result['num_requests'] += result[4]
                else:       # result will be lists of results. 1, 2, 3 will be lists themselves
                    self.variant_result['exec_time'].append(result[0])  # list

                    # for the following 3, append list of the single costs to serve the whole sequence
                    self.variant_result['swaps'].append(result[1])  # variant result is a dict having lists of lists
                    self.variant_result['adjust'].append(result[2])     # result [i] is a list
                    self.variant_result['access'].append(result[3])        # variant_result['cost'] is a list of lists
                    self.variant_result['num_requests'].append(result[4])

        end = time.time()

        # compute means
        if self.incr_req:
            req_means = dict()
            for x in range(0, self.num_simulations):
                req_dict = self.per_sequence_dict[x]        # get dict of all requests for this sequence
                for i in range(0, len(req_dict)):           # iterate over request dict and add values to req_means
                    if i in req_means:
                        req_means[i]['time'].append(req_dict[i][0])     # req_dict[i][0] = first result for req i
                        req_means[i]['swaps'].append(req_dict[i][1])
                        req_means[i]['adjust'].append(req_dict[i][2])
                        req_means[i]['access'].append(req_dict[i][3])
                    else:
                        req_means[i] = dict()
                        req_means[i]['time'] = req_dict[i][0]
                        req_means[i]['swaps'] = req_dict[i][1]
                        req_means[i]['adjust'] = req_dict[i][2]
                        req_means[i]['access'] = req_dict[i][3]

            print("finished " + algo_type + ", #" + str(self.num_simulations) + " seq for max " + str(len(req_means)) + " req")
            return req_means
        else:               # incr_net experiment
            if self.means:
                for x in self.cumulative_result:
                    self.cumulative_result[x] = int(self.cumulative_result[x] / self.num_simulations)       # take means of experiments

                return [time.asctime(time.localtime(start)), time.asctime(time.localtime(end)), int((end - start)*1000),
                        self.cumulative_result['mean_exec_time'], self.cumulative_result['mean_swaps'],
                        self.cumulative_result['mean_adjust'],
                        self.cumulative_result['mean_access'],
                        self.cumulative_result['num_requests']]
            else:   # these contain all results, lists of lists are returned
                return[time.asctime(time.localtime(start)), time.asctime(time.localtime(end)), int((end - start)*1000),
                       self.variant_result['exec_time'], self.variant_result['swaps'],
                       self.variant_result['adjust'], self.variant_result['access'],
                       self.variant_result['num_requests']]

 # https://stackoverflow.com/questions/8595973/truncate-to-three-decimals-in-python
def truncate(number, digits):
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def count_nodes(set):
    """returns counter: the set of distinct nodes (hosts) in the sequence"""
    counter = dict()
    # print(set)
    for item in set:
        if item in counter:
            counter[item] += 1
        else:
            counter[item] = 0
    return counter

def correct_n_hosts(sequence, n_hosts):
    """add hosts to reach n_hosts and return set of distinct nodes"""
    diff = n_hosts-len(count_nodes(sequence))
    new_seq = None
    if diff > 0:
        print("Diff")
        print(diff)
        new_seq = sequence.copy()
        counter = 0
        while counter < diff:
            added = random.randint(1, n_hosts)
            if added not in new_seq:
                new_seq.append(added)
                counter += 1
        distinct = []
        for x in count_nodes(new_seq):
            distinct.append(x)
        new_seq = distinct
    if new_seq:
        return new_seq
    else: return []
