import importlib

import matplotlib.pyplot as plt; plt.rcdefaults()
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

def plot_q1_access(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in res_for_net[5][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        rotate.append(res_for_net[5][i][j])     # list cost of serving
            elif a == "max":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        max_alg.append(res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        static.append(res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        random.append(res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        half.append(res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        splay.append(res_for_net[5][i][j])
        saving_dict = dict()
        saving_dict['src_db'] = res_for_net[7]
        saving_dict['entropy'] = res_for_net[8]
        saving_dict['tested_temp_p'] = res_for_net[9]
        saving_dict['total_time'] = res_for_net[1]
        saving_dict['mean_exec_time'] = res_for_net[2]
        saving_dict['mean_adjust'] = res_for_net[4]
        saving_dict['mean_swaps'] = res_for_net[3]
        saving_dict['mean_access'] = res_for_net[5]

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q1_access.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                           'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                           'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q1_access_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q1_adjust(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j])      # adjust + swaps
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j])
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q1_adjust.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q1_adjust_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q1_total(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j]+res_for_net[5][i][j])
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q1_total.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("src_db")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q1_total_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q2_access(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in res_for_net[5][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        rotate.append(res_for_net[5][i][j]) 
                        # print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        max_alg.append(res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        static.append(res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        random.append(res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        half.append(res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        splay.append(res_for_net[5][i][j])
            saving_dict = dict()
            saving_dict['temp_p'] = round(res_for_net[7])
            saving_dict['entropy'] = res_for_net[8]
            saving_dict['tested_temp_p'] = res_for_net[9]
            saving_dict['total_time'] = res_for_net[1]
            saving_dict['mean_exec_time'] = res_for_net[2]
            saving_dict['mean_adjust'] = res_for_net[4]
            saving_dict['mean_swaps'] = res_for_net[3]
            saving_dict['mean_access'] = res_for_net[5]

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q2_access.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                           'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                           'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q2_access_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q2_adjust(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j])      # adjust + swaps
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j])
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q2_adjust.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q2_adjust_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q2_total(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            #if n not in groups:
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(n)
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j]+res_for_net[5][i][j])
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q2_total.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n = " + str(n_hosts) + ", r = " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("temporal_p")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q2_total_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q3_access(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific entropy
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in res_for_net[5][i]:
                        groups.append(round(n,2))
            if a == "rotate":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        rotate.append(res_for_net[5][i][j])
                        # print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        max_alg.append(res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        static.append(res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        random.append(res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        half.append(res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        splay.append(res_for_net[5][i][j])

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q3_access.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                           'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                           'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q3_access_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q3_adjust(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific entropy
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(round(n,2))
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j])      # adjust + swaps
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j])
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q3_adjust.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q3_adjust_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q3_total(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific entropy
            #if n not in groups:
            if a == "rotate":
                for i in range(0,len(res_for_net[4])):
                    for j in res_for_net[4][i]:
                        groups.append(round(n,2))
            if a == "rotate":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j]+res_for_net[5][i][j])
                        #print(j)
            elif a == "max":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        max_alg.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "static_offline":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        static.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "random":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        random.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "half_opt":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        half.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])
            elif a == "splay":
                for i in range(0, len(res_for_net[4])):
                    for j in range(0, len(res_for_net[4][i])):
                        splay.append(res_for_net[4][i][j] + res_for_net[3][i][j]+res_for_net[5][i][j])

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Splay': splay, 'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q3_total.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                       'Static': static})
    df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("entropy")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q3_total_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)


def plot_q4_access(data, n_hosts, n_req):      # groups = various, value_vars = algos
    groups = []
    rotate = []
    for res in data:    # loop for each result (different experiments: temporal_p, entropy, real data, weibull)
        for a in res:  # loop for each algo
            one_algo = res[a]  # get all data for this algo
            for n in one_algo:
                res_for_net = one_algo[n]  # take res-list for specific experiment-classification
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in res_for_net[5][i]:
                            groups.append(n)
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            rotate.append(res_for_net[5][i][j])
                            # print(j)
    # print("Served")
    # print(len(groups))
    # print(len(rotate))

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate})
    df = df[['Group', 'Rotate']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("access cost, n= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q4_access.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q4_adjust(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    for res in data:  # loop for each result (different experiments: temporal_p, entropy, real data, weibull)
        for a in res:  # loop for each algo
            one_algo = res[a]  # get all data for this algo
            list_of_dicts = []
            for n in one_algo:
                res_for_net = one_algo[n]  # take res-list for specific experiment-classification
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in res_for_net[5][i]:
                            groups.append(n)
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j])
                            # print(j)
    # print("Served")
    # print(len(groups))
    # print(len(rotate))

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate})
    df = df[['Group', 'Rotate']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("adjust cost, n= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q4_adjust.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q4_total(data, n_hosts, n_req):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    for res in data:  # loop for each result (different experiments: temporal_p, entropy, real data, weibull)
        for a in res:  # loop for each algo
            one_algo = res[a]  # get all data for this algo
            list_of_dicts = []
            for n in one_algo:
                res_for_net = one_algo[n]  # take res-list for specific experiment-classification
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in res_for_net[5][i]:
                            groups.append(n)
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            rotate.append(res_for_net[4][i][j]+res_for_net[3][i][j]+res_for_net[5][i][j])
                            # print(j)
    # print("Served")
    # print(len(groups))
    # print(len(rotate))

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate})
    df = df[['Group', 'Rotate']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("total cost, n=" + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("cost")
    pl.set_xlabel("")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q4_total.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

def plot_q5_access(data, n_hosts, n_req):      # groups = various, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    oblivious =[]
    max_alg = []
    for res in data:    # loop for each result (different experiments: temporal_p, entropy, real data, weibull)
        for a in res:  # loop for each algo
            one_algo = res[a]  # get all data for this algo
            for n in one_algo:
                res_for_net = one_algo[n]  # take res-list for specific experiment-classification
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in res_for_net[5][i]:
                            groups.append(n)
                if a == "rotate":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            rotate.append(res_for_net[5][i][j])
                            # print(j)
                elif a == "max":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            max_alg.append(res_for_net[5][i][j])
                elif a == "static_offline":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            static.append(res_for_net[5][i][j])
                elif a == "random":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            random.append(res_for_net[5][i][j])
                elif a == "half_opt":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            half.append(res_for_net[5][i][j])
                elif a == "splay":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            splay.append(res_for_net[5][i][j])
                elif a == "static_oblivious":
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            oblivious.append(res_for_net[5][i][j])
    # print("Served")
    # print(len(groups))
    # print(len(rotate))
    rotate_diff = []
    random_diff = []
    half_diff = []
    splay_diff = []
    static_diff = []
    oblivious_diff = []
    for i in range(0, len(max_alg)):
        rotate_diff.append(max_alg[i] - rotate[i])
        random_diff.append(max_alg[i] - random[i])
        half_diff.append(max_alg[i] - half[i])
        splay_diff.append(max_alg[i] - splay[i])
        static_diff.append(max_alg[i] - static[i])      # max = 5 .. static = 10 -> negative
        oblivious_diff.append(max_alg[i] - oblivious[i])

    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate_diff, 'Random': random_diff, 'Half': half_diff,
                       'Splay': splay_diff, 'Static': static_diff, 'Oblivious': oblivious_diff})
    df = df[['Group', 'Rotate', 'Random', 'Half', 'Splay', 'Static', 'Oblivious']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Random', 'Half', 'Splay', 'Static', 'Oblivious'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    #sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("diff. to max-height, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("difference")
    pl.set_xlabel("")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    # plt.figure(figsize=(12, 8))
    plt.savefig("q5_MRU.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)
    df = pd.DataFrame({'Group': groups,
                       'Rotate': rotate_diff, 'Random': random_diff, 'Half': half_diff,
                    'Static': static_diff, 'Oblivious': oblivious_diff})
    df = df[['Group', 'Rotate', 'Random', 'Half', 'Static', 'Oblivious']]
    dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Random', 'Half', 'Static', 'Oblivious'],
                 var_name='algos')
    pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
    #sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
    pl.set_title("diff. to max-height, n(max)= " + str(n_hosts) + ", r= " + str(n_req))
    pl.set_ylabel("difference")
    pl.set_xlabel("")
    plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
    plt.savefig("q5_MRU_nosplay.png", bbox_inches='tight')
    importlib.reload(plt)
    importlib.reload(sns)

"""def plot_q2_swaps(data):      # groups = temp_p, value_vars = algos
    groups = []
    rotate = []
    random = []
    half = []
    splay = []
    static = []
    max_alg = []
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            #if n not in groups:
            if a == "rotate":
                for x in res_for_net[3]:
                    for j in x:
                        groups.append(n)
            if a == "rotate":
                for x in res_for_net[3]:    # list of lists (list of sequences, each seq has a list of costs)
                    for j in x:
                        rotate.append(j)
                        #print(j)
            elif a == "max":
                for x in res_for_net[3]:
                    for j in x:
                        max_alg.append(j)
            elif a == "static_offline":
                for x in res_for_net[3]:
                    for j in x:
                        static.append(j)
            elif a == "random":
                for x in res_for_net[3]:
                    for j in x:
                        random.append(j)
            elif a == "half_opt":
                for x in res_for_net[3]:
                    for j in x:
                        half.append(j)
            elif a == "splay":
                for x in res_for_net[3]:
                    for j in x:
                        if j <= 200:
                            splay.append(j)
                        else:
                            splay.append(0)
    print("Served")
    print(len(groups))
    print(len(rotate))
    # print(len(random))
    # print(len(max_alg))
    # print(len(static))
    # print(len(splay))
    add_splay = True
    for i in range(1):  # do 2x
        if add_splay:
            df = pd.DataFrame({'Group': groups,
                               'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                               'Splay': splay, 'Static': static})
            df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static']]
            dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Splay', 'Static'],
                         var_name='algos')
        else:
            df = pd.DataFrame({'Group': groups,
                               'Rotate': rotate, 'Random': random, 'Max': max_alg, 'Half': half,
                               'Static': static})
            df = df[['Group', 'Rotate', 'Max', 'Random', 'Half', 'Static']]
            dd = pd.melt(df, id_vars=['Group'], value_vars=['Rotate', 'Max', 'Random', 'Half', 'Static'],
                         var_name='algos')
        pl = sns.boxplot(x='Group', y='value', data=dd, hue='algos', showfliers=False)
        sns.pointplot(x='Group', y='value', data=dd.groupby('Group', as_index=False).mean(), ax=pl)
        pl.set_title("swaps")
        pl.set_ylabel("cost")
        pl.set_xlabel("temporal_p")
        plt.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0.)
        # plt.figure(figsize=(12, 8))
        if add_splay:
            plt.savefig("swaps.png", bbox_inches='tight')
            importlib.reload(plt)
            importlib.reload(sns)
        else: plt.savefig("swaps_nosplay.png", bbox_inches='tight')
        add_splay = False"""

def plot_q1():
    # data to plot
    n_groups = 4
    max = (90, 55, 40, 65)
    static = (85, 62, 54, 20)
    half = (85, 62, 54, 20)
    rotate = (85, 62, 54, 20)
    random = (85, 62, 54, 20)

    # create plot
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35
    opacity = 0.8

    rects1 = plt.bar(index, means_frank, bar_width,
                     alpha=opacity,
                     color='b',
                     label='Frank')

    rects2 = plt.bar(index + bar_width, means_guido, bar_width,
                     alpha=opacity,
                     color='g',
                     label='Guido')

    plt.xlabel('Person')
    plt.ylabel('Scores')
    plt.title('Scores by person')
    plt.xticks(index + bar_width, ('A', 'B', 'C', 'D'))
    plt.legend()

    plt.tight_layout()
    plt.show()