import collections

from scipy.stats import powerlaw
from scipy.stats import exponweib
import scipy.stats as stats
import numpy as np
from numpy import random
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

def generate_zipf_sequences():
    n_list = [15, 63, 255, 1023, 4095, 65535]
    for n in n_list:
        print("Generating for n = " + str(n))
        generate_zipf_for_n(n)

def generate_zipf_for_n(n_hosts):
    # source: https://stackoverflow.com/questions/33331087/sampling-from-a-bounded-domain-zipf-distribution
    N = n_hosts
    sequence_size = 1000000
    json_file_name = f"zipf-seq/spatial-zipf-dataset-N{N}-seq{sequence_size}.json"
    x = np.arange(1, N + 1)
    a_parameter_values = [1.001, 1.3, 1.6, 1.9, 2.2]
    repetitions = 1

    dataset = {}

    for a in a_parameter_values:
        weights = x ** (-a)
        weights /= weights.sum()
        bounded_zipf = stats.rv_discrete(name='bounded_zipf', values=(x, weights))
        dataset[a] = {}

        # repeat n-times
        for i in range(repetitions):
            sample = list(bounded_zipf.rvs(size=sequence_size))
            sample_large_ids = list(map(lambda curr_id: curr_id*99999, sample))
            dataset[a][f'sample{i}'] = {}
            dataset[a][f'sample{i}']['sequence'] = str(sample_large_ids)
            dataset[a][f'sample{i}']['entropy'] = empirical_entropy(sample_large_ids)
        # plot only the 10th data distribution
        #plt.hist(sample, bins=np.arange(1, N + 2))
        #plt.show()
    f = open(json_file_name, "w")
    f.close()

    with open(json_file_name, "w") as handle:
        json.dump(dataset, handle)

    # loading the dataset
    with open(json_file_name, "r") as handle:
        loaded_dataset = json.load(handle)

    # convert sequence from string to list
    for a in loaded_dataset:
        for i in range(repetitions):
            loaded_dataset[a][f'sample{i}']['sequence'] = ast.literal_eval(loaded_dataset[a][f'sample{i}']['sequence'])

def sorted_nodes_occur(x):
    counter = dict()
    for item in x:
        if item in counter:
            counter[item] += 1
        else:
            counter[item] = 1
    listofTuples = sorted(counter.items(), key=lambda x: x[1])
    return listofTuples

def empirical_entropy(seq):
    frequency = {}

    # count occurencies
    for element in seq:
        if element not in frequency:
            frequency[element] = 1
        else:
            frequency[element] += 1

    # divide by seq size
    for element in frequency:
        frequency[element] = frequency[element] / len(seq)

    # compute entropy
    entropy = 0
    for element in frequency:
        entropy -= frequency[element] * np.log2(frequency[element])

    return entropy
