#!/usr/bin/python
# -*- coding: latin-1 -*-
import ast
import json

from bar_plotter import plot_bar, plot_bar_nosplay, plot_bar_q4, plot_rot_vs_rand
from data_handler import save_res_dict, save_seq_dict, get_zipf_hosts
from experiment import *
import multi_experiment as mx

import decimal
import logging
from threeDplotter import *

"""
File designed for running experiments with artificially generated data
In case of temporal experiments, it can generate data on the fly (set parameter)
"""

# 1) experiments used for the paper:
def run_temporal_incr_tree():
    selected_p = [0.1, 0.5, 0.9]
    while selected_p:
        run_incr_tree_for_temporal(selected_p.pop(0))

def run_incr_tree_for_temporal(passed_temp_p):
    """run experiment for different tree sizes and selected temporal localities"""
    algos = ["min", "random", "rotate", "static_offline", "half_opt", "max", "splay", "static_oblivious"]
    num_simulations = 10
    temporal_p = passed_temp_p
    size_dataset = 1000000
    means = False    # True: result is means of total costs, False: result is all costs to serve each req
    single_results = False
    if means == False:
        single_results = True
    n_hosts = [255, 1023, 4095, 16383, 65535]
    res_dict = dict()  # dict for storing all results of the exp
    seq_writer = dict()  # dict for storing the sequences used by the exp
    
    # run for each incrementing n_hosts-size
    while n_hosts:
        incr_net = n_hosts.pop(0)

        # generate data on the go
        params = [num_simulations, incr_net, size_dataset]
        set_params_q2(params)
        params = [num_simulations, size_dataset, incr_net, temporal_p, 1]
        set_params_nodes_unchanged(params, exp_type="temp_p")
        seq_writer[incr_net] = dict()
        for i in range(0, len(sequences_array)):
            temp = sequences_array[i].copy()
            large_ids = list(map(lambda number: number * 99999, temp))
            seq_writer[incr_net][i] = large_ids
        exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req=False,
                         means=means, n_hosts=incr_net)
        entropy_set = calc_entropy(sequences_array)
        temporal_p_set = calc_temporal_locality(sequences_array, True)
        entropy = 0
        for e in entropy_set:
            entropy += e
        entropy /= len(entropy_set)  # take mean of entropies
        temp_p = 0
        for p in temporal_p_set:
            temp_p += p
        temp_p /= len(temporal_p_set)
        print("Run exp for incr_net  " + str(incr_net))

        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=single_results)

            # prepare for csv-write
            del res[0]            # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])
            res.append(incr_net)
            res.append(round(entropy,2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]][incr_net] = res      # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()         # create dict for this algo
                res_dict[algos[i]][incr_net] = res
    res_dict["added_param"] = str(passed_temp_p)
    seq_writer["added_param"] = str(passed_temp_p)
    save_res_dict("temp_incr_tree", res_dict.copy())
    save_seq_dict("temp_incr_tree", seq_writer.copy())
    print("Plotting")
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="temp_incr_tree", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=False)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="temp_incr_tree", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=True)
    plot_bar(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="temp_incr_tree", stand_dev=False, use_median=False, acc_only=True)

def run_incr_tree_zipf():
    z_params = ["1.001", "1.6", "2.2"]
    while z_params:
        run_incr_tree_for_zipf(z_params.pop(0))

# run experiment for different tree sizes and selected spatial localities (zipf)
def run_incr_tree_for_zipf(zipf_param):
    """run experiment using external zipf data
    param num_simulations not needed since it results from the already generated data"""
    algos = ["random", "rotate", "half_opt", "static_offline", "max", "splay", "static_oblivious"]
    size_dataset = 1000000
    n_hosts = [63, 255, 1023, 4095, 16383, 65535]
    res_dict = dict()  # dict for storing all results of the exp
    seq_writer = dict()
    means = False  # True: result is means of total costs, False: result is all costs to serve each req

    # run for each incrementing part of dataset
    while n_hosts:
        curr_n_hosts = n_hosts.pop(0)
        set_nodes(curr_n_hosts)
        z_dist = get_zipf_hosts(n_hosts=curr_n_hosts, n_req = size_dataset)
        sequences_array = []
        num_simulations = 1
        for i in range(0, len(z_dist[zipf_param])):     # go through all samples for this param
            if not num_simulations: break
            num_simulations-=1
            data_str = z_dist[zipf_param]["sample"+str(i)]["sequence"]
            seq = ast.literal_eval(data_str)
            sequences_array.append(seq)
        print(len(sequences_array))
        seq_writer[curr_n_hosts] = dict()
        for i in range(0, len(sequences_array)):
            seq_writer[curr_n_hosts][i] = sequences_array[i]
        exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req=False,
                         means=means, n_hosts=curr_n_hosts)
        entropy_set = calc_entropy(sequences_array)
        temporal_p_set = calc_temporal_locality(sequences_array, artificial=True)
        entropy = 0
        for e in entropy_set:
            entropy += e
        entropy /= len(entropy_set)  # take mean of entropies
        temp_p = 0
        for p in temporal_p_set:
            temp_p += p
        temp_p /= len(temporal_p_set)
        print("Run exp for incr_net=  " + str(curr_n_hosts))
        print("entropy: " + str(entropy))
        
        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=True)

            # prepare for json-write
            del res[0]  # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])
            res.append(curr_n_hosts)
            res.append(round(entropy, 2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]][curr_n_hosts] = res  # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()  # create dict for this algo
                res_dict[algos[i]][curr_n_hosts] = res

    res_dict["added_param"] = zipf_param
    seq_writer["added_param"] = zipf_param
    save_res_dict("zipf_incr_tree", res_dict.copy())
    save_seq_dict("zipf_incr_tree", seq_writer.copy())
    print("Plotting")
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="zipf_incr_tree", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=False)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="zipf_incr_tree", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=True)
    plot_bar(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="zipf_incr_tree", stand_dev=False, use_median=False, acc_only=True)

def create_increasing_p_data():
    incr_p_set = [0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9]
    seq_writer = dict()
    while incr_p_set:
        incr_p = incr_p_set.pop(0)
        params = [num_simulations, size_dataset, n_hosts, incr_p, 1]
        set_params_nodes_unchanged(params, "temp_p")
        seq_writer[incr_p] = dict()
        for i in range(0, len(sequences_array)):
            seq_writer[incr_p][i] = sequences_array[i]
    save_seq_dict("q2", seq_writer)

# run experiment increasing the temporal locality p (Q2)
def run_art_increasing_p():
    """ Run for different temporal p's, for a determined (params) network and sequence size """
    algos = ["random", "rotate", "static_offline", "half_opt", "max", "splay", "static_oblivious"]
    res_dict = dict()  # dict for storing all results of the exp
    means = False   # True: result is means of total costs, False: result is all costs to serve each req
    incr_p_set = [0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9]
    seq_writer = dict()
    generate_on_the_go = False
    imported_dataset = None
    if not generate_on_the_go:       # sequences have been generated before
        with open("plots/q2_seq.json") as f_in:
            imported_dataset = {float(k): dict(v) for k, v in json.load(f_in).items()}
            for i in imported_dataset:
                imported_dataset[i] = {int(k): list(v) for k, v in imported_dataset[i].items()}
            print(imported_dataset)

    # run for each incrementing temporal_p, use same set of sequences for all p's
    while incr_p_set:
        incr_p = incr_p_set.pop(0)
        temp = None
        if generate_on_the_go:
            params = [num_simulations, size_dataset, n_hosts, incr_p, 1]
            set_params_nodes_unchanged(params, "temp_p")
        else:
            temp = imported_dataset[incr_p]
        sequences_array = temp
        seq_writer[incr_p] = dict()
        for i in range(0, len(sequences_array)):
            if generate_on_the_go:
                seq_writer[incr_p][i] = sequences_array[i]
            else:
                seq_writer[incr_p][i] = sequences_array[i]
        exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req = False,
                         means=means, n_hosts=n_hosts)
        entropy = 0
        temp_p = 0
        if generate_on_the_go:
            entropy_set = calc_entropy(sequences_array)
            temporal_p_set = calc_temporal_locality(sequences_array, artificial=True)
            for e in entropy_set:
                entropy += e
            entropy /= len(entropy_set)  # take mean of entropies
            for p in temporal_p_set:
                temp_p += p
            temp_p /= len(temporal_p_set)
            print("Final temp" + str(temp_p))
            print("Entropy " + str(entropy))
        print("Run exp for incr_p  " + str(incr_p))

        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=True)

            # prepare for csv-write
            del res[0]            # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])     # insert name of algo
            res.append(round(incr_p,2))
            res.append(round(entropy,2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]][round(incr_p,2)] = res      # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()         # create dict for this algo
                res_dict[algos[i]][round(incr_p,2)] = res
    save_seq_dict("q2", seq_writer)
    save_res_dict("q2", res_dict)
    print("Plotting Q2")
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q2", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=False)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q2", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=True)
    plot_bar(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q2", stand_dev=False, use_median=False,acc_only=True)

def run_incr_zipf():
    """run experiment using external zipf data - Q3"""
    algos = ["random", "rotate", "half_opt", "static_offline", "max", "splay", "static_oblivious"]
    size_dataset = 50000
    n_hosts = 15
    set_nodes(n_hosts)
    res_dict = dict()  # dict for storing all results of the exp
    means = False  # True: result is means of total costs, False: result is all costs to serve each req
    #zipf1000 = get_zipf(1000)
    #zipf50000 = get_zipf(50000)
    zipfMil = get_zipf_hosts(n_hosts=n_hosts, n_req = size_dataset)
    z_params = []
    for z in zipfMil:
        z_params.append(z)

    # run for each incrementing part of dataset
    while z_params:
        z = z_params.pop(0)
        sequences_array = []
        for i in range(0, len(zipfMil[z])):
            data_str = zipfMil[z]["sample"+str(i)]["sequence"]
            seq = ast.literal_eval(data_str)
            sequences_array.append(seq)
        exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req=False,
                         means=means, n_hosts=n_hosts)
        entropy_set = calc_entropy(sequences_array)
        temporal_p_set = calc_temporal_locality(sequences_array, artificial=True)
        entropy = 0
        for e in entropy_set:
            entropy += e
        entropy /= len(entropy_set)  # take mean of entropies
        temp_p = 0
        for p in temporal_p_set:
            temp_p += p
        temp_p /= len(temporal_p_set)
        print("Run exp for z=  " + str(z))
        print("entropy: " + str(entropy))
        
        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=True)

            # prepare for json-write
            del res[0]  # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])
            res.append(z)
            res.append(round(entropy, 2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]][z] = res  # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()  # create dict for this algo
                res_dict[algos[i]][z] = res
    save_res_dict("q3_2", res_dict)
    print("Plotting")
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q3_2", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=False)
    plot_bar_nosplay(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q3_2", stand_dev=False, use_median=False,
                     acc_only=False, adj_only=True)
    plot_bar(res_dict, n_hosts=n_hosts, n_req=size_dataset, q="q3_2", stand_dev=False, use_median=False, acc_only=True)

def run_incr_rotor_vs_obl_using_zipf():
    """run experiment for zipf processed with different temporal ps
        for each dataset-combination, create num_simulations-sequences and serve them with rotor and oblivious"""
    algos = ["rotate", "static_oblivious"]
    sel_temp_ps = [0, 0.25, 0.5, 0.75, 0.9]
    size_dataset = 1000000
    n_hosts = 65535
    num_simulations = 10
    set_nodes(n_hosts)
    res_dict = dict()  # dict for storing all results of the exp
    means = False  # True: result is means of total costs, False: result is all costs to serve each req
    zipfMil = get_zipf_hosts(n_hosts=n_hosts, n_req = size_dataset)
    z_params = []
    for z in zipfMil:
        z_params.append(z)
    seq_writer = dict()
    while z_params:     # run for each incrementing part of dataset
        z = z_params.pop(0)
        sequences_array = []
        for i in range(0, len(zipfMil[z])):
            data_str = zipfMil[z]["sample" + str(i)]["sequence"]
            seq = ast.literal_eval(data_str)
            sequences_array.append(seq)
        print(len(sequences_array))
        postprocessed_s = []
        seq_writer[z] = dict()
        for p in sel_temp_ps:  # postprocess all sequences for first p parameter, then for second, ...
            seq_writer[z][p] = dict()
            for i in range(0,len(sequences_array)):  # num_simulation sequences

                # set current temp_p for postprocess, ignore param "weibull"
                params = ["weibull", num_simulations, size_dataset, n_hosts, p]  
                set_parameters_no_generation(params)
                postpr_seq = adapt_temporal_locality(sequences_array[i])
                postprocessed_s.append(postpr_seq)
                seq_writer[z][p][i] = postpr_seq

        exp = experiment(tree_nodes=tree_nodes, sequences_array=postprocessed_s, artificial=True, incr_req=False,
                         means=means, n_hosts=n_hosts)
        entropy_set = calc_entropy(postprocessed_s)
        temporal_p_set = calc_temporal_locality(postprocessed_s, artificial=True)
        entropy = 0
        for e in entropy_set:
            entropy += e
        entropy /= len(entropy_set)  # take mean of entropies
        temp_p = 0
        for p in temporal_p_set:
            temp_p += p
        temp_p /= len(temporal_p_set)
        print("Run exp for z=  " + str(z))

        # run an experiment for each algorithm on the given topologies
        for i in range(len(algos)):
            res = exp.run(algo_type=algos[i], single_results=True)

            # prepare for json-write
            del res[0]  # remove start- and end-time
            del res[0]
            res.insert(0, algos[i])
            res.append(z)
            res.append(round(entropy, 2))
            res.append(round(temp_p, 2))
            if algos[i] in res_dict:
                res_dict[algos[i]][z] = res  # assign the result for net_size and algo
            else:
                res_dict[algos[i]] = dict()  # create dict for this algo
                res_dict[algos[i]][z] = res
    new_res_dict = dict()
    for res_algo in res_dict:    # for each algo: 6*5*1 = 300 simulations (6 zipf * 5 p * 10)
        new_res_dict[res_algo] = dict()
        for zipf_dist in res_dict[res_algo]:     # for each zipf_dist: 50 simulations
            new_res_dict[res_algo][zipf_dist] = dict()  # now create further dict to differentiate p's
            access_list_of_cost_lists = res_dict[res_algo][zipf_dist][5]
            adjust_list_of_cost_lists = res_dict[res_algo][zipf_dist][4]
            swaps_list_of_cost_lists = res_dict[res_algo][zipf_dist][3]
            count_simul = 0
            stops = [10, 20, 30, 40, 50]    # first ten simulations are for first p-param
            for temporal_dist in sel_temp_ps:       # do 5 times
                stop = stops.pop(0)
                new_res_dict[res_algo][zipf_dist][temporal_dist] = dict()
                new_res_dict[res_algo][zipf_dist][temporal_dist]["acc"] = []
                new_res_dict[res_algo][zipf_dist][temporal_dist]["adj"] = []
                new_res_dict[res_algo][zipf_dist][temporal_dist]["swaps"] = []
                while count_simul != stop:       # stop when reaching 10, 20, ...
                    new_res_dict[res_algo][zipf_dist][temporal_dist]["acc"].append(
                        access_list_of_cost_lists[count_simul])
                    new_res_dict[res_algo][zipf_dist][temporal_dist]["adj"].append(
                        adjust_list_of_cost_lists[count_simul])
                    new_res_dict[res_algo][zipf_dist][temporal_dist]["swaps"].append(
                        swaps_list_of_cost_lists[count_simul])
                    count_simul += 1
    save_seq_dict("q4", seq_writer)
    save_res_dict("q4", new_res_dict)
    print("Plotting")
    plot_q4_3d(new_res_dict, n_hosts, size_dataset, wire=True)

def run_rotor_vs_random():
    """run experiment for comparison btw. rotor and random
            datasets: high temporal + uniform"""
    algos = ["rotate", "random"]
    num_simulations = 1
    n_simul = 10
    #size_dataset = 1000000
    #n_hosts = 50000
    size_dataset = 1000
    n_hosts = 50
    set_nodes(n_hosts)
    all_res = dict()
    while n_simul:
        res_dict = dict()
        temporal_p_set = [0]
        while temporal_p_set:
            p = temporal_p_set.pop(0)
            #res_dict[p] = dict()
            params = [num_simulations, size_dataset, n_hosts, p, 0]
            set_params_nodes_unchanged(params, "temp_p")
            exp = experiment(tree_nodes=tree_nodes, sequences_array=sequences_array, artificial=True, incr_req=True,
                             means=True, n_hosts=n_hosts)

            for i in range(len(algos)):
                print("Run exp for p = " + str(p))
                res = exp.run(algo_type=algos[i], single_results=False)
                if algos[i] in res_dict:
                    res_dict[algos[i]][p] = res  # assign the result for p and algo
                else:
                    res_dict[algos[i]] = dict()  # create dict for this algo
                    res_dict[algos[i]][p] = res
        n_simul-=1
        all_res[10-n_simul] = res_dict
    save_res_dict("rot-rand_all_data", all_res)
    plot_rot_vs_rand(size_dataset)


# 2) helper functions

def set_parameters_no_generation(params):
    global distribution, demand_sequence, size_dataset, sequences_array, n_hosts, size_dataset, temporal_p
    [distribution, num_simulations, size_dataset, n_hosts, temporal_p] = params

# generate data for the temporal_p experiment
def set_nodes(n_hosts):
    global tree_nodes
    tree_nodes = []

    # generate new data-set
    tree_nodes = create_nodes(n_hosts)
    return tree_nodes

def set_params_q2(params):
    global size_dataset, n_hosts, num_simulations, tree_nodes
    [num_simulations, n_hosts, size_dataset] = params

    tree_nodes = set_nodes(n_hosts)

def set_params_nodes_unchanged(params, exp_type):
    """change global params without affecting the sequence"""
    global distribution, demand_sequence, size_dataset, sequences_array, n_hosts, num_simulations, temporal_p, incr_part
    [num_simulations, size_dataset, n_hosts, temporal_p, incr_part] = params
    sequences_array = []
    if exp_type == "temp_p":        # generate new sequences out of original dataset
        for i in range(0, num_simulations):
            new_t_seq = []
            while len(new_t_seq) < size_dataset:
                r = random.randint(0, n_hosts-1)      # pick hosts randomly and add them to sequence
                request = tree_nodes[r]
                new_t_seq.append(request)
            sequences_array.append(adapt_temporal_locality(new_t_seq))      # postprocess sequence
    elif exp_type == "entropy":
        new_n_hosts = n_hosts * incr_part

        # generate new sequences out of original dataset
        for i in range(0, num_simulations):
            new_hosts = []
            while len(new_hosts) < new_n_hosts:     # select hosts that will be placed in this sequence
                r = random.randint(0, n_hosts-1)
                if tree_nodes[r] not in new_hosts:
                    new_hosts.append(tree_nodes[r])
            new_seq = []
            while len(new_seq) < size_dataset:      # create sequence using the specified hosts only
                r = random.randint(0, len(new_hosts)-1)
                new_seq.append(new_hosts[r])
            sequences_array.append(new_seq)

def create_nodes(size):
    """
    :param size: number of hosts to generate
    :return: randomly distributed hosts
    """
    nodes = []
    for i in range(1, size+1):
        nodes.append(i)
    res = random.sample(nodes, len(nodes))
    return res

def count_nodes(set):
    """returns counter: the set of distinct nodes (hosts) in the sequence"""
    counter = dict()
    #print(set)
    for item in set:
        if item in counter:
            counter[item] += 1
        else:
            counter[item] = 0
    return counter


def calc_entropy(seq_array):
    entropy = 0
    entropy_set =[]
    for s in seq_array:
        frequency_set = dg.sorted_nodes_occur(s)
        for i in range(len(frequency_set)):
            fx = frequency_set[i][1]/len(s)
            #print("fx" +str(fx) +" len " +str(len(s)) +" frequency " +str(frequency_set[i][1]))
            entropy += fx*log2(1/fx)
        entropy_set.append(entropy)
        entropy = 0
    return entropy_set

def calc_temporal_locality(seq_array, artificial):
    counter = 0
    p_set = []
    for s in seq_array:
        if artificial:
            curr = s[0]
            for i in range(1, len(s)):
                if s[i] == curr:
                    counter += 1
                curr = s[i]
        else:
            flat_s = [item for sublist in s for item in sublist]    # flatten list of lists
            curr = flat_s[0]
            for i in range(1, len(flat_s)):
                if flat_s[i] == curr:
                    counter += 1
                curr = flat_s[i]
        p_set.append(counter/len(s))
        counter = 0
    return p_set

def calc_spatial_locality(seq, n_items):
    distinct_items = dg.sorted_nodes_occur(seq)
    #n_items = len(distinct_items)
    threshold = n_items * 0.05
    freq_occur_items = 0
    for x in distinct_items:
        if x[1] > threshold:
            freq_occur_items += 1
    return freq_occur_items/n_items

def adapt_temporal_locality(sequence):
    """(Avin et al. : On the Complexity of Traffic Traces and Implications)
At each step, we add a new request to the trace: with probability p, we repeat the last request.
With probability (1 ? p), we sample a new request from the sequence."""
    p = temporal_p             # repeating probability p: the probability of sampling the last request
    if p == 0:
        return sequence
    trace = []
    original = sequence.copy()
    trace.append(original.pop(0))      # start by sampling the first pair from M.
    curr = trace[0]
    while original:         # if original size = 500, do 500 times
        if repeat(p):       # do with probability p
            trace.append(curr)
        else:       # pick next item
            trace.append(original[0])
            curr = original[0]      # update curr to new item
        original.pop(0)     # goto next item
    return trace

# return True if random number smaller than p
def repeat(probability):
    return random.random() < probability
