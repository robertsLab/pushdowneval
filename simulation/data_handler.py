import json

def save_seq_dict(q, dict):
    dest = 'plots/'
    if q == "q1":
        dest += 'q1_seq.json'
    elif q == "q1_fb":
        dest += 'q1_fb_seq.json'
    elif q == "q2":
        dest += 'q2_seq.json'
    elif q == "q3":
        dest += 'q3_seq.json'
    elif q == "q3_1_seq":
        dest += 'q3_1_seq.json'
    elif q == "q3_2":
        dest += 'q3_2_seq.json'
    elif q == "q4":
        dest += 'q4_seq.json'
    elif q == "temp_incr_tree":
        dest += dict["added_param"]
        dest += 'temp_incr_tree_seq.json'
        del dict["added_param"]
    elif q == "zipf_incr_tree":
        dest += dict["added_param"]
        dest += 'zipf_incr_tree_seq.json'
        del dict["added_param"]
    with open(dest, 'w+') as f:
        # use json.dump(dict, f, indent=4) to "pretty-print" with four spaces per indent
        json.dump(dict, f)

def save_res_dict(q, dict):
    dest = 'plots/'
    if q == "q1":
        dest += 'q1.json'
    elif q == "q1_fb":
        dest += 'q1_fb.json'
    elif q == "q2":
        dest += 'q2.json'
    elif q == "q3":
        dest += 'q3.json'
    elif q == "q3_1":
        dest += 'q3_1.json'
    elif q == "q3_2":
        dest += 'q3_2.json'
    elif q == "q4":
        dest += 'q4.json'
    elif q == "small":
        dest += 'small_diff.json'
    elif q == "small_all_data":
        dest += 'small_all_data.json'
    elif q == "temp_incr_tree":
        dest += dict["added_param"]
        dest += 'temp_incr_tree.json'
        del dict["added_param"]
    elif q == "zipf_incr_tree":
        dest += dict["added_param"]
        dest += 'zipf_incr_tree.json'
        del dict["added_param"]
    elif q == "rot-rand_all_data":
        dest += "rot-rand_all_data.json"
    with open(dest, 'w+') as f:
        # use json.dump(dict, f, indent=4) to "pretty-print" with four spaces per indent
        json.dump(dict, f)

def get_zipf_hosts(n_hosts, n_req):
    filename = ""
    req_param = 0
    if n_req == 1000000 or n_req == 50000:
        req_param = n_req
    else:
        raise Exception("No sequence generated for the wished number of requests")
    if n_hosts == 15:
        filename = "zipf-seq/spatial-zipf-dataset-N15-seq"+str(req_param)+".json"
    elif n_hosts == 63:
        filename = "zipf-seq/spatial-zipf-dataset-N63-seq"+str(req_param)+".json"
    elif n_hosts == 255:
        filename = "zipf-seq/spatial-zipf-dataset-N255-seq"+str(req_param)+".json"
    elif n_hosts == 1023:
        filename = "zipf-seq/spatial-zipf-dataset-N1023-seq"+str(req_param)+".json"
    elif n_hosts == 4095:
        filename = "zipf-seq/spatial-zipf-dataset-N4095-seq"+str(req_param)+".json"
    elif n_hosts == 16383:
        filename = "zipf-seq/spatial-zipf-dataset-N16383-seq"+str(req_param)+".json"
    elif n_hosts == 65535:
        filename = "zipf-seq/spatial-zipf-dataset-N65535-seq"+str(req_param)+".json"
    else:
        raise Exception("error, file not found for n_hosts-input")
    with open(filename) as f_in:
        return json.load(f_in)

def get_calgary_original(sample):        # n_hosts = number of words
    words = []
    source = ""             # > 100k req on 1k nodes
    if sample == 0:
        source = 'calgary_sets/bible_letters_.txt'
    elif sample == 1:
        source = 'calgary_sets/book1_letters_'
    elif sample == 2:
        source = 'calgary_sets/news_letters_'
    elif sample == 3:
        source = 'calgary_sets/plrabn12_letters_.txt'
    elif sample == 4:
        source = 'calgary_sets/world192_letters_.txt'
    with open(source) as f:
        for line in f:
            res = [''.join(c for c in word if c.isalpha()) for word in line.split()] # for each line, fetch the words if they are chars
            for word in res:
                words.append(word)
    return words

def count_nodes(set):
    """returns counter: the set of distinct nodes (hosts) in the sequence"""
    counter = dict()
    for item in set:
        if item in counter:
            counter[item] += 1
        else:
            counter[item] = 0
    return counter