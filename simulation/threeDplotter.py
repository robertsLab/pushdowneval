import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator


def plot_q4_3d(data, n_hosts, n_req, acc_only=False, adj_only=False, rotate_only=False, obl_only=False, wire=False, weib=False):
    res_means = dict()
    for algo in data:       # data looks like: data[res_algo][weib_dist][temporal_dist]["acc"] = [ 10 simulation lists ]
        res_means[algo] = dict()
        for dist in data[algo]:
            res_means[algo][dist] = dict()
            # if dist not in x_ax:   # each dist holds 5 temporal_dists
            #     x_ax.append(dist)
            for temporal_dist in data[algo][dist]:
                res_means[algo][dist][temporal_dist] = dict()
                res_means[algo][dist][temporal_dist]["acc"] = []
                res_means[algo][dist][temporal_dist]["adj"] = []
                res_means[algo][dist][temporal_dist]["tot"] = []

                for cost in data[algo][dist][temporal_dist]:    # temporal_dist holds 3 lists (acc,adj,swaps)== cost
                    # each cost contains 10 lists, each list is a simulation
                    if cost == "acc":
                        for l_s in data[algo][dist][temporal_dist][cost]:      # l_s == list of costs for serving one sequence
                            seq_mean_arr = []
                            for x in l_s:
                                seq_mean_arr.append(x)
                            res = np.mean(seq_mean_arr)     # mean to serve one sequence
                            res_means[algo][dist][temporal_dist]["acc"].append(res)
                    if cost == "adj":
                        for i in range(0, len(data[algo][dist][temporal_dist][cost])):
                            seq_mean_arr = []
                            for j in range(0, len(data[algo][dist][temporal_dist][cost][i])):
                                seq_mean_arr.append(data[algo][dist][temporal_dist][cost][i][j]
                                                  + data[algo][dist][temporal_dist]["swaps"][i][j])
                            res = np.mean(seq_mean_arr)  # mean to serve one sequence
                            res_means[algo][dist][temporal_dist]["adj"].append(res)

                            # also compute total cost (with access)
                            tot_mean_arr = []
                            for j in range(0, len(data[algo][dist][temporal_dist][cost][i])):
                                tot_mean_arr.append(data[algo][dist][temporal_dist][cost][i][j]
                                                  + data[algo][dist][temporal_dist]["swaps"][i][j]
                                                    + data[algo][dist][temporal_dist]["acc"][i][j])
                            tot_res = np.mean(tot_mean_arr)
                            res_means[algo][dist][temporal_dist]["tot"].append(tot_res)
    # [ 10 means ] -> compute the means of the means
    means_of_means = dict()

    #print(res_means["rotate"][0.5][0.1]["acc"])
    for algo in res_means:
        means_of_means[algo] = dict()
        for dist in res_means[algo]:
            means_of_means[algo][dist] = dict()
            for temporal in res_means[algo][dist]:
                for cost in res_means[algo][dist][temporal]:
                    means_of_means[algo][dist][cost] = dict()   # final dict

    for algo in res_means:
        for dist in res_means[algo]:
            for temporal in res_means[algo][dist]:
                for cost in res_means[algo][dist][temporal]:
                    if not means_of_means[algo][dist][cost]:
                        means_of_means[algo][dist][cost] = []  # this list will contain 5 means for temporal locality
                    one_dist_mean = []
                    for prev_mean in res_means[algo][dist][temporal][cost]:   # here the previous means can be found
                        one_dist_mean.append(prev_mean)
                    means_of_means[algo][dist][cost].append(sum(one_dist_mean)/len(one_dist_mean))  # calc mean of means
                    # list for each distribution: values for temporal locality

    # for algo in means_of_means:
    #     for dist in means_of_means[algo]:
    #             print("Dist: " +str(dist))
    #             print(means_of_means[algo][dist])

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    y = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0]
    x = [0.1, 0.3, 0.5, 0.7, 0.9]
    if not weib:
        x = [0, 0.25, 0.5, 0.75, 0.9]
        y = [1.001, 1.3, 1.6, 1.9, 2.2]

    X, Y = np.meshgrid(x, y)
    ax = plt.axes(projection='3d')
    if weib:
        ax.set_ylabel('alpha')
    else: ax.set_ylabel('zipf-a')
    ax.set_xlabel('temporal_p')
    ax.set_xticks(x)
    ax.set_yticks(y)
    y_descr = ['1.001', '1.3', '1.6', '1.9', '2.2']
    x_descr = [0, 0.25, 0.5, 0.75, 0.9]
    ax.set_yticklabels(y_descr, fontsize = 10)
    ax.set_xticklabels(x_descr, fontsize = 10)

    Z_temp = []
    filename="plots/q4"
    if not rotate_only and not obl_only:    # plot difference
        if acc_only:
            ax.set_title("Rotate-Oblivious access-diff, n = " + str(n_hosts) + ", r = " + str(n_req))
            filename += "_R-O_acc"
            for algo in means_of_means:
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "acc":
                                diff_list = []
                                for i in range(0, len(means_of_means[algo][dist][cost])):
                                    diff = means_of_means["rotate"][dist][cost][i] \
                                           - means_of_means["static_oblivious"][dist][cost][i]
                                    diff_list.append(diff)
                                Z_temp.append(diff_list)
        elif adj_only:
            filename += "_R-O_adj"
            ax.set_title("Rotate-Oblivious adjust-diff, n = " + str(n_hosts) + ", r = " + str(n_req))
            for algo in means_of_means:
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "adj":
                                diff_list = []
                                for i in range(0, len(means_of_means[algo][dist][cost])):
                                    diff = means_of_means["rotate"][dist][cost][i] \
                                           - means_of_means["static_oblivious"][dist][cost][i]
                                    diff_list.append(diff)
                                Z_temp.append(diff_list)
        else:       # plot total difference
            filename += "_R-O_tot"
            #ax.set_title("Rotate-Oblivious total-diff, n = " + str(n_hosts) + ", r = " + str(n_req))
            for algo in means_of_means:
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "tot":
                                diff_list = []
                                for i in range(0, len(means_of_means[algo][dist][cost])):
                                    diff = means_of_means["rotate"][dist][cost][i] \
                                           - means_of_means["static_oblivious"][dist][cost][i]
                                    diff_list.append(diff)
                                Z_temp.append(diff_list)

    # one of the algos is selected
    elif acc_only:
        for algo in means_of_means:
            if rotate_only:
                filename += "_rot_acc"
                ax.set_title("Rotate access, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "acc":
                                Z_temp.append(means_of_means[algo][dist][cost])
            elif obl_only:
                filename += "_obl_acc"
                ax.set_title("Static_oblivious access, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "static_oblivious":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "acc":
                                Z_temp.append(means_of_means[algo][dist][cost])

    elif adj_only:
        for algo in means_of_means:
            if rotate_only:
                filename += "_rot_adj"
                ax.set_title("Rotate adjust, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "adj":
                                Z_temp.append(means_of_means[algo][dist][cost])
            elif obl_only:
                filename += "_obl_adj"
                ax.set_title("Static_oblivious adjust, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "static_oblivious":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "adj":
                                Z_temp.append(means_of_means[algo][dist][cost])
    else:       # plot total cost
        for algo in means_of_means:
            if rotate_only:
                filename += "_rot_tot"
                ax.set_title("Rotate total, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "rotate":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "tot":
                                Z_temp.append(means_of_means[algo][dist][cost])
            elif obl_only:
                filename += "_obl_tot"
                #ax.set_title("Static_oblivious total, n = " + str(n_hosts) + ", r = " + str(n_req))
                if algo == "static_oblivious":
                    for dist in means_of_means[algo]:
                        for cost in means_of_means[algo][dist]:
                            if cost == "tot":
                                Z_temp.append(means_of_means[algo][dist][cost])
    Z_dumb = Z_temp.copy()
    Z = np.array(Z_temp)
    print(Z)
    print(X)
    print(Y)
    for i in range(0, len(Z_dumb)):
        for j in range(0, len(Z_dumb[i])):
            Z_dumb[i][j] = 0
    if wire:
        filename += "_wire"
        # surf2 = ax.plot_surface(X, Y, np.array(Z_dumb), shade=False)
        # surf2.set_facecolor((0.8, 0, 0.0, 0.8))
        norm = plt.Normalize(Z.min(), 0)
        colors = cm.Blues(norm(Z))
        rcount, ccount, _ = colors.shape
        surf = ax.plot_surface(X, Y, Z, rcount=rcount, ccount=ccount,
                               facecolors=colors, shade=False)
        surf.set_facecolor((0.0, 0, 0.0, 0.6))
        ax.view_init(15, 40)
        #surf.set_facecolor((0, 0, 0.3, 0.6))
        #fig.colorbar(surf, shrink=0.5, aspect=5)
        # ax.plot_wireframe(X, Y, Z, color='green')
        # #ax.plot_wireframe(X, Y, np.array(Z_dumb), color='black')
        ax.set_zlabel('cost', rotation='vertical')
    else:
        surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter('{x:.02f}')
        fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.savefig(filename+".pdf")
    #fig.show()