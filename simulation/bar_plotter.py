import importlib
import json
from statistics import median

import matplotlib.pyplot as plt;
from matplotlib import pyplot
from matplotlib import cm

import matplotlib as mpl

plt.rcdefaults()
import numpy as np

def plot_bar_nosplay(origin_data, n_hosts, n_req, q, stand_dev, use_median, acc_only, adj_only=False):      # groups = temp_p, value_vars = algos
    groups = []
    res_for_groups = dict()
    data = origin_data.copy()
    added_param = ""        # temporal_p in case of temporal incr_tree experiment, zipf in case of zipf-dist incre_tree
    if q == "temp_incr_tree" or q == "zipf_incr_tree":
        added_param = data["added_param"]
        del data["added_param"]
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        list_of_dicts = []
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if n not in groups:
                groups.append(n)
                res_for_groups[n] = dict()          # dict of dicts
            if a == "rotate":
                res_for_groups[n]["rotate_acc"] = []
                res_for_groups[n]["rotate_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["rotate_acc"].append(res_for_net[5][i][j])     # list cost of serving
                        res_for_groups[n]["rotate_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #rotate["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j]+res_for_net[5][i][j])
            elif a == "max":
                res_for_groups[n]["max_acc"] = []
                res_for_groups[n]["max_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["max_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["max_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #max_alg["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "static_offline":
                res_for_groups[n]["static_acc"] = []
                res_for_groups[n]["static_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["static_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["static_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #static["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "static_oblivious":
                res_for_groups[n]["oblivious_acc"] = []
                res_for_groups[n]["oblivious_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["oblivious_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["oblivious_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
            elif a == "random":
                res_for_groups[n]["random_acc"] = []
                res_for_groups[n]["random_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["random_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["random_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #random["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "half_opt":
                res_for_groups[n]["half_acc"] = []
                res_for_groups[n]["half_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["half_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["half_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #half["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "splay":
                pass

    # each algo needs to have list of resp. means
    rotate_acc_means = []
    rotate_acc_std = []
    rotate_adj_means = []
    rotate_adj_std = []

    max_acc_means = []
    max_acc_std = []
    max_adj_means = []
    max_adj_std = []

    random_acc_means = []
    random_acc_std = []
    random_adj_means = []
    random_adj_std = []

    half_acc_means = []
    half_acc_std = []
    half_adj_means = []
    half_adj_std = []

    static_acc_means = []
    static_acc_std = []
    static_adj_means = []
    static_adj_std = []

    oblivious_acc_means = []
    oblivious_acc_std = []
    oblivious_adj_means = []
    oblivious_adj_std = []

    for n in res_for_groups:
        if use_median:
            rotate_acc_means.append(median(res_for_groups[n]["rotate_acc"]))
            rotate_adj_means.append(median(res_for_groups[n]["rotate_adj"]))
            static_acc_means.append(median(res_for_groups[n]["static_acc"]))
            static_adj_means.append(median(res_for_groups[n]["static_adj"]))
            half_acc_means.append(median(res_for_groups[n]["half_acc"]))
            half_adj_means.append(median(res_for_groups[n]["half_adj"]))
            max_acc_means.append(median(res_for_groups[n]["max_acc"]))
            max_adj_means.append(median(res_for_groups[n]["max_adj"]))
            random_acc_means.append(median(res_for_groups[n]["random_acc"]))
            random_adj_means.append(median(res_for_groups[n]["random_adj"]))
            oblivious_acc_means.append(median(res_for_groups[n]["oblivious_acc"]))
            oblivious_adj_means.append(median(res_for_groups[n]["oblivious_adj"]))
        else:
            rotate_acc_means.append(sum(res_for_groups[n]["rotate_acc"]) / len(res_for_groups[n]["rotate_acc"]))
            rotate_adj_means.append(sum(res_for_groups[n]["rotate_adj"]) / len(res_for_groups[n]["rotate_adj"]))
            static_acc_means.append(sum(res_for_groups[n]["static_acc"]) / len(res_for_groups[n]["static_acc"]))
            static_adj_means.append(sum(res_for_groups[n]["static_adj"]) / len(res_for_groups[n]["static_adj"]))
            half_acc_means.append(sum(res_for_groups[n]["half_acc"]) / len(res_for_groups[n]["half_acc"]))
            half_adj_means.append(sum(res_for_groups[n]["half_adj"]) / len(res_for_groups[n]["half_adj"]))
            max_acc_means.append(sum(res_for_groups[n]["max_acc"]) / len(res_for_groups[n]["max_acc"]))
            max_adj_means.append(sum(res_for_groups[n]["max_adj"]) / len(res_for_groups[n]["max_adj"]))
            random_acc_means.append(sum(res_for_groups[n]["random_acc"]) / len(res_for_groups[n]["random_acc"]))
            random_adj_means.append(sum(res_for_groups[n]["random_adj"]) / len(res_for_groups[n]["random_adj"]))
            oblivious_acc_means.append(
                sum(res_for_groups[n]["oblivious_acc"]) / len(res_for_groups[n]["oblivious_acc"]))
            oblivious_adj_means.append(
                sum(res_for_groups[n]["oblivious_adj"]) / len(res_for_groups[n]["oblivious_adj"]))

        rotate_acc_std.append(np.std(res_for_groups[n]["rotate_acc"]))
        rotate_adj_std.append(np.std(res_for_groups[n]["rotate_adj"]))

        static_acc_std.append(np.std(res_for_groups[n]["static_acc"]))
        static_adj_std.append(np.std(res_for_groups[n]["static_adj"]))

        half_acc_std.append(np.std(res_for_groups[n]["half_acc"]))
        half_adj_std.append(np.std(res_for_groups[n]["half_adj"]))

        max_acc_std.append(np.std(res_for_groups[n]["max_acc"]))
        max_adj_std.append(np.std(res_for_groups[n]["max_adj"]))

        random_acc_std.append(np.std(res_for_groups[n]["random_acc"]))
        random_adj_std.append(np.std(res_for_groups[n]["random_adj"]))

        oblivious_acc_std.append(np.std(res_for_groups[n]["oblivious_acc"]))
        oblivious_adj_std.append(np.std(res_for_groups[n]["oblivious_adj"]))

    # print(oblivious_acc_means)
    # print(static_acc_means)
    x = np.arange(len(groups))
    labels = groups
    width = 0.13  # the width of the bars: can also be len(x) sequence
    if q == "q3":
        width = 0.12
    if adj_only:
        width = 0.15

    fig, ax = plt.subplots()
    n_lines = 6
    c = np.arange(1, n_lines + 1)

    norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])

    patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-')
    if acc_only:
        if stand_dev:
            ax.bar(x, rotate_acc_means, width, yerr=rotate_acc_std, label='Rotor_access', )
            ax.bar(x + width, random_acc_means, width, yerr=random_acc_std, label='Random_access', )
            ax.bar(x + width * 2, half_acc_means, width, yerr=half_acc_std, label='Half_access', )
            ax.bar(x + width * 3, max_acc_means, width, yerr=max_acc_std, label='Max_access', )
            ax.bar(x + width * 4, oblivious_acc_means, width, yerr=oblivious_acc_std, label='Static_oblivious', )
            ax.bar(x + width * 5, static_acc_means, width, yerr=static_acc_std, label='Static_opt', )
        else:
            ax.bar(x, rotate_acc_means, width, label='Rotor_access', hatch=patterns[0], color='w', edgecolor='k')
            ax.bar(x + width, random_acc_means, width, label='Random_access', hatch=patterns[1], color='w',
                   edgecolor='k')
            ax.bar(x + width * 2, half_acc_means, width, label='Half_access', hatch=patterns[2], color='w',
                   edgecolor='k')
            ax.bar(x + width * 3, max_acc_means, width, label='Max_access', hatch=patterns[3], color='w', edgecolor='k')
            ax.bar(x + width * 5, oblivious_acc_means, width, label='Static_oblivious', hatch=patterns[5], color='w',
                   edgecolor='k')
            ax.bar(x + width * 6, static_acc_means, width, label='Static_opt', hatch=patterns[6], color='w',
                   edgecolor='k')
    elif adj_only:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
        if stand_dev:
            ax.bar(x, rotate_adj_means, width, yerr=rotate_adj_std, label='Rotor_adjust', )
            ax.bar(x + width, random_adj_means, width, yerr=random_adj_std, label='Random_adjust', )
            ax.bar(x + width * 2, half_adj_means, width, yerr=half_adj_std, label='Half_adjust', )
            ax.bar(x + width * 3, max_adj_means, width, yerr=max_adj_std, label='Max_adjust', )
        else:
            ax.bar(x, rotate_adj_means, width, label='Rotor_adjust', hatch=patterns[2],color=cmap.to_rgba(0 + 1), edgecolor='k')
            ax.bar(x + width, random_adj_means, width, label='Random_adjust', hatch=patterns[6], color=cmap.to_rgba(1.2 + 1), edgecolor='k')
            ax.bar(x + width * 2, half_adj_means, width, label='Half_adjust', hatch=patterns[0], color=cmap.to_rgba(1.9 + 1), edgecolor='k')
            ax.bar(x + width * 3, max_adj_means, width, label='Max_adjust', hatch=patterns[1], color=cmap.to_rgba(2.4 + 1), edgecolor='k')
    else:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
        if stand_dev:
            ax.bar(x, rotate_acc_means, width, yerr=rotate_acc_std, label='Rotor_access', )
            ax.bar(x, rotate_adj_means, width, yerr=rotate_adj_std, label='Rotor_adjust', bottom=rotate_acc_means)

            ax.bar(x + width, random_acc_means, width, yerr=random_acc_std, label='Random_access', )
            ax.bar(x + width, random_adj_means, width, yerr=random_adj_std, label='Random_adjust', bottom=random_acc_means)

            ax.bar(x + width * 2, half_acc_means, width, yerr=half_acc_std, label='Half_access', )
            ax.bar(x + width * 2, half_adj_means, width, yerr=half_adj_std, label='Half_adjust', bottom=half_acc_means)

            ax.bar(x + width * 3, max_acc_means, width, yerr=max_acc_std, label='Max_access', )
            ax.bar(x + width * 3, max_adj_means, width, yerr=max_adj_std, label='Max_adjust', bottom=max_acc_means)

            ax.bar(x + width * 4, oblivious_acc_means, width, yerr=oblivious_acc_std, label='Static_oblivious', )

            ax.bar(x + width * 5, static_acc_means, width, yerr=static_acc_std, label='Static_opt', )


        else:
            ax.bar(x, rotate_acc_means, width, label='Rotor_access', hatch=patterns[0], color=cmap.to_rgba(0 + 1), edgecolor='k')
            ax.bar(x, rotate_adj_means, width, label='Rotor_adjust', bottom=rotate_acc_means, hatch=patterns[2],color=cmap.to_rgba(0 + 1), edgecolor='k')

            ax.bar(x + width, random_acc_means, width, label='Random_access', hatch=patterns[1], color=cmap.to_rgba(1 + 1),edgecolor='k')
            ax.bar(x + width, random_adj_means, width, label='Random_adjust', bottom=random_acc_means,hatch=patterns[6], color=cmap.to_rgba(1 + 1), edgecolor='k')

            ax.bar(x + width * 2, half_acc_means, width, label='Half_access', hatch=patterns[2], color=cmap.to_rgba(1.5 + 1),edgecolor='k')
            ax.bar(x + width * 2, half_adj_means, width, label='Half_adjust', bottom=half_acc_means, hatch=patterns[0], color=cmap.to_rgba(1.5 + 1), edgecolor='k')

            # ax.bar(x + width * 3, splay_acc_means, width, label='Splay_access', hatch=patterns[3], color='w', edgecolor='k')
            # ax.bar(x + width * 3, splay_adj_means, width, label='Splay_adjust', bottom=splay_acc_means)

            ax.bar(x + width * 3, max_acc_means, width, label='Max_access', hatch=patterns[3], color=cmap.to_rgba(1.9 + 1), edgecolor='k')
            ax.bar(x + width * 3, max_adj_means, width, label='Max_adjust', bottom=max_acc_means, hatch=patterns[1],color=cmap.to_rgba(1.9 + 1), edgecolor='k')

            ax.bar(x + width * 4, oblivious_acc_means, width, label='Static_oblivious', hatch=patterns[5], color=cmap.to_rgba(2.5 + 1),edgecolor='k')

            ax.bar(x + width * 5, static_acc_means, width, label='Static_opt', hatch=patterns[6], color=cmap.to_rgba(2.9 + 1), edgecolor='k')

    if adj_only:
        ax.set_xticks(x + width + width / 2)
    else:
        ax.set_xticks(x + 2*width + width / 2)
    ax.set_xticklabels(labels)
    ax.set_ylabel('Cost', fontsize="19")
    # if acc_only:
    #     ax.set_title("Access, n = " + str(n_hosts) + ", r = " + str(n_req))
    if adj_only or acc_only:
        #ax.set_title("Adjust, n = " + str(n_hosts) + ", r = " + str(n_req))
        ax.legend(bbox_to_anchor=(0, -0.3), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
    else:
        plt.rcParams.update({'font.size': 19})
        if q == "q1" or q == "q1_fb" or q == "temp_incr_tree" or q == "zipf_incr_tree":
            ax.legend(bbox_to_anchor=(0, -0.4), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
        else:
            ax.legend(loc='upper right', ncol=1, fontsize=19)
        # ax.set_title("Access + adjustment, n = " + str(n_hosts) + ", r = " + str(n_req))
    fig.set_figwidth(10)
    fig.set_figheight(10)
    plt.rcParams.update({'font.size': 19})
    ax.tick_params(axis='x', which='major', labelsize=19)
    ax.tick_params(axis='y', which='major', labelsize=19)
    #ax.legend(bbox_to_anchor=(0, -0.2), loc='lower left', borderaxespad=0., ncol=3, prop={'size': 15}, fontsize=22)

    #
    filename = "plots/bar"
    if use_median:
        plot_type = "_median_nosplay"
    else:
        plot_type = "_mean_nosplay"
    if q == "q1":
        filename += "_q1"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        filename += "_noerr"
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "q1_fb":
        filename += "_q1_fb"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        filename += "_noerr"
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "q2":
        filename += "_q2"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Temporal p', fontsize="17")
        filename += "_noerr"
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "q3":
        filename += "_q3"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Entropy', fontsize="17")
        if stand_dev:
            plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
        else:
            filename += "_noerr"
            plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
    elif q == "q3_1":
        filename += "_q3_1"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Alpha', fontsize="17")
        if stand_dev:
            plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
        else:
            filename += "_noerr"
            plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
    elif q == "q3_2":
        filename += "_q3_2"
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Zipf-a', fontsize="17")
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "temp_incr_tree":
        filename += "_temp_incr_tree"+str(added_param)
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Tree size', fontsize="17")
        #ax.set_title("Temporal p = " + str(n_hosts) + ", r = " + str(n_req))
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
        plot_bar_diff_obliv(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means, random_acc_means,
                            max_adj_means,
                            max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means)
        plot_bar_diff_optimal(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means,
                              random_acc_means,
                              max_adj_means,
                              max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means)
    elif q == "zipf_incr_tree":
        filename += "_zipf_incr_tree"+str(added_param)
        if acc_only:
            filename += "_acc"
        elif adj_only:
            filename += "_adj"
        ax.set_xlabel('Tree size', fontsize="17")
        #ax.set_title("Temporal p = " + str(n_hosts) + ", r = " + str(n_req))
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
        plot_bar_diff_obliv(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means, random_acc_means,
                      max_adj_means,
                      max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means)
        plot_bar_diff_optimal(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means, random_acc_means,
                            max_adj_means,
                            max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means)
    plot_bar_std_dev(q, acc_only, groups, rotate_acc_std, rotate_adj_std, random_acc_std, random_adj_std, half_acc_std,
                     half_adj_std, None, None, max_acc_std, max_adj_std,
                     oblivious_acc_std, oblivious_adj_std, static_acc_std, static_adj_std)

def plot_bar_diff_obliv(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means, random_acc_means, max_adj_means,
                  max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means):
    patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
    fig, ax = plt.subplots()
    n_lines = 6
    c = np.arange(1, n_lines + 1)

    norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])
    x = np.arange(len(groups))
    labels = groups
    width = 0.15  # the width of the bars: can also be len(x) sequence

    fig.set_figwidth(10)
    fig.set_figheight(10)
    plt.rcParams.update({'font.size': 19})
    ax.set_xticks(x + 2 * width + width / 2)
    ax.set_xticklabels(labels)

    rotate_diff = []
    random_diff = []
    max_diff = []
    half_diff = []

    for i in range(0,len(oblivious_acc_means)):
        rotate_diff.append(rotate_acc_means[i]+rotate_adj_means[i]-oblivious_acc_means[i])
        random_diff.append(random_acc_means[i]+random_adj_means[i] - oblivious_acc_means[i])
        max_diff.append(max_acc_means[i]+max_adj_means[i] - oblivious_acc_means[i])
        half_diff.append(half_acc_means[i] + half_adj_means[i] - oblivious_acc_means[i])

    ax.bar(x, rotate_diff, width, label='Rotor', hatch=patterns[0], color=cmap.to_rgba(0 + 1),
       edgecolor='k')
    ax.bar(x + width, random_diff, width, label='Random', hatch=patterns[1], color=cmap.to_rgba(1 + 1),
           edgecolor='k')
    ax.bar(x + width * 2, half_diff, width, label='Half', hatch=patterns[2], color=cmap.to_rgba(1.5 + 1),
           edgecolor='k')
    ax.bar(x + width * 3, max_diff, width, label='Max', hatch=patterns[3], color=cmap.to_rgba(1.9 + 1),
           edgecolor='k')
    ax.set_ylabel('Difference', fontsize="19")
    ax.set_xlabel('Tree size', fontsize="17")
    ax.tick_params(axis='x', which='major', labelsize=19)
    ax.tick_params(axis='y', which='major', labelsize=19)

    ax.legend(bbox_to_anchor=(0, -0.2), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
    filename = "plots/bar_incr_tree_diff_obliv"
    filename += str(added_param)
    plt.savefig(filename + ".pdf", bbox_inches='tight')

def plot_bar_diff_optimal(added_param, groups, rotate_acc_means, rotate_adj_means, random_adj_means, random_acc_means, max_adj_means,
                  max_acc_means, half_adj_means, half_acc_means, oblivious_acc_means, static_acc_means):
    patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
    fig, ax = plt.subplots()
    n_lines = 6
    c = np.arange(1, n_lines + 1)

    norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])
    x = np.arange(len(groups))
    labels = groups
    width = 0.13  # the width of the bars: can also be len(x) sequence

    fig.set_figwidth(10)
    fig.set_figheight(10)
    plt.rcParams.update({'font.size': 19})
    ax.set_xticks(x + 2 * width + width / 2)
    ax.set_xticklabels(labels)

    rotate_diff = []
    random_diff = []
    max_diff = []
    half_diff = []

    for i in range(0,len(oblivious_acc_means)):
        rotate_diff.append(rotate_acc_means[i]+rotate_adj_means[i]-static_acc_means[i])
        random_diff.append(random_acc_means[i]+random_adj_means[i] - static_acc_means[i])
        max_diff.append(max_acc_means[i]+max_adj_means[i] - static_acc_means[i])
        half_diff.append(half_acc_means[i] + half_adj_means[i] - static_acc_means[i])

    ax.bar(x, rotate_diff, width, label='Rotor', hatch=patterns[0], color=cmap.to_rgba(0 + 1),
       edgecolor='k')
    ax.bar(x + width, random_diff, width, label='Random', hatch=patterns[1], color=cmap.to_rgba(1 + 1),
           edgecolor='k')
    ax.bar(x + width * 2, half_diff, width, label='Half', hatch=patterns[2], color=cmap.to_rgba(1.5 + 1),
           edgecolor='k')
    ax.bar(x + width * 3, max_diff, width, label='Max', hatch=patterns[3], color=cmap.to_rgba(1.9 + 1),
           edgecolor='k')
    ax.set_ylabel('Difference', fontsize="19")
    ax.set_xlabel('Tree size', fontsize="17")
    ax.tick_params(axis='x', which='major', labelsize=19)
    ax.tick_params(axis='y', which='major', labelsize=19)

    ax.legend(bbox_to_anchor=(0, -0.2), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
    filename = "plots/bar_incrtree_diff_opt_"
    filename += str(added_param)
    plt.savefig(filename + ".pdf", bbox_inches='tight')

def plot_bar(origin_data, n_hosts, n_req, q, stand_dev, use_median, acc_only):      # groups = temp_p, value_vars = algos
    groups = []
    res_for_groups = dict()
    data = origin_data
    added_param = ""
    if q == "temp_incr_tree" or q == "zipf_incr_tree":
        added_param = data["added_param"]
        del data["added_param"]
    for a in data:  # loop for each algo
        one_algo = data[a]  # get all data for this algo
        for n in one_algo:
            res_for_net = one_algo[n]  # take res-list for specific p
            if n not in groups:
                groups.append(n)
                res_for_groups[n] = dict()          # dict of dicts
            if a == "rotate":
                res_for_groups[n]["rotate_acc"] = []
                res_for_groups[n]["rotate_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["rotate_acc"].append(res_for_net[5][i][j])     # list cost of serving
                        res_for_groups[n]["rotate_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #rotate["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j]+res_for_net[5][i][j])
            elif a == "max":
                res_for_groups[n]["max_acc"] = []
                res_for_groups[n]["max_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["max_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["max_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #max_alg["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "static_offline":
                res_for_groups[n]["static_acc"] = []
                res_for_groups[n]["static_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["static_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["static_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #static["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "static_oblivious":
                res_for_groups[n]["oblivious_acc"] = []
                res_for_groups[n]["oblivious_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["oblivious_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["oblivious_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
            elif a == "random":
                res_for_groups[n]["random_acc"] = []
                res_for_groups[n]["random_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["random_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["random_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #random["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "half_opt":
                res_for_groups[n]["half_acc"] = []
                res_for_groups[n]["half_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["half_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["half_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #half["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])
            elif a == "splay":
                res_for_groups[n]["splay_acc"] = []
                res_for_groups[n]["splay_adj"] = []
                for i in range(0, len(res_for_net[5])):
                    for j in range(0, len(res_for_net[5][i])):
                        res_for_groups[n]["splay_acc"].append(res_for_net[5][i][j])  # list cost of serving
                        res_for_groups[n]["splay_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])
                        #splay["total"].append(res_for_net[3][i][j] + res_for_net[4][i][j] + res_for_net[5][i][j])



    saving_dict = dict()
    saving_dict['src_db'] = res_for_net[7]
    saving_dict['entropy'] = res_for_net[8]
    saving_dict['tested_temp_p'] = res_for_net[9]
    saving_dict['total_time'] = res_for_net[1]
    saving_dict['mean_exec_time'] = res_for_net[2]
    saving_dict['mean_adjust'] = res_for_net[4]
    saving_dict['mean_swaps'] = res_for_net[3]
    saving_dict['mean_access'] = res_for_net[5]

    # each algo needs to have list of resp. means
    rotate_acc_means = []
    rotate_acc_std = []
    rotate_adj_means = []
    rotate_adj_std = []

    max_acc_means = []
    max_acc_std = []
    max_adj_means = []
    max_adj_std = []

    random_acc_means = []
    random_acc_std = []
    random_adj_means = []
    random_adj_std = []

    half_acc_means = []
    half_acc_std = []
    half_adj_means = []
    half_adj_std = []

    splay_acc_means = []
    splay_acc_std = []
    splay_adj_means = []
    splay_adj_std = []

    static_acc_means = []
    static_acc_std = []
    static_adj_means = []
    static_adj_std = []

    oblivious_acc_means = []
    oblivious_acc_std = []
    oblivious_adj_means = []
    oblivious_adj_std = []

    for n in res_for_groups:
        if use_median:
            rotate_acc_means.append(median(res_for_groups[n]["rotate_acc"]))
            rotate_adj_means.append(median(res_for_groups[n]["rotate_adj"]))
            static_acc_means.append(median(res_for_groups[n]["static_acc"]))
            static_adj_means.append(median(res_for_groups[n]["static_adj"]))
            half_acc_means.append(median(res_for_groups[n]["half_acc"]))
            half_adj_means.append(median(res_for_groups[n]["half_adj"]))
            max_acc_means.append(median(res_for_groups[n]["max_acc"]))
            max_adj_means.append(median(res_for_groups[n]["max_adj"]))
            random_acc_means.append(median(res_for_groups[n]["random_acc"]))
            random_adj_means.append(median(res_for_groups[n]["random_adj"]))
            oblivious_acc_means.append(median(res_for_groups[n]["oblivious_acc"]))
            oblivious_adj_means.append(median(res_for_groups[n]["oblivious_adj"]))
            splay_acc_means.append(median(res_for_groups[n]["splay_acc"]))
            splay_adj_means.append(median(res_for_groups[n]["splay_adj"]))
        else:
            rotate_acc_means.append(sum(res_for_groups[n]["rotate_acc"]) / len(res_for_groups[n]["rotate_acc"]))
            rotate_adj_means.append(sum(res_for_groups[n]["rotate_adj"]) / len(res_for_groups[n]["rotate_adj"]))
            static_acc_means.append(sum(res_for_groups[n]["static_acc"]) / len(res_for_groups[n]["static_acc"]))
            static_adj_means.append(sum(res_for_groups[n]["static_adj"]) / len(res_for_groups[n]["static_adj"]))
            half_acc_means.append(sum(res_for_groups[n]["half_acc"]) / len(res_for_groups[n]["half_acc"]))
            half_adj_means.append(sum(res_for_groups[n]["half_adj"]) / len(res_for_groups[n]["half_adj"]))
            max_acc_means.append(sum(res_for_groups[n]["max_acc"]) / len(res_for_groups[n]["max_acc"]))
            max_adj_means.append(sum(res_for_groups[n]["max_adj"]) / len(res_for_groups[n]["max_adj"]))
            random_acc_means.append(sum(res_for_groups[n]["random_acc"]) / len(res_for_groups[n]["random_acc"]))
            random_adj_means.append(sum(res_for_groups[n]["random_adj"]) / len(res_for_groups[n]["random_adj"]))
            oblivious_acc_means.append(
                sum(res_for_groups[n]["oblivious_acc"]) / len(res_for_groups[n]["oblivious_acc"]))
            oblivious_adj_means.append(
                sum(res_for_groups[n]["oblivious_adj"]) / len(res_for_groups[n]["oblivious_adj"]))
            splay_acc_means.append(sum(res_for_groups[n]["splay_acc"]) / len(res_for_groups[n]["splay_acc"]))
            splay_adj_means.append(sum(res_for_groups[n]["splay_adj"]) / len(res_for_groups[n]["splay_adj"]))

        splay_acc_std.append(np.std(res_for_groups[n]["splay_acc"]))
        splay_adj_std.append(np.std(res_for_groups[n]["splay_adj"]))

        rotate_acc_std.append(np.std(res_for_groups[n]["rotate_acc"]))
        rotate_adj_std.append(np.std(res_for_groups[n]["rotate_adj"]))

        static_acc_std.append(np.std(res_for_groups[n]["static_acc"]))
        static_adj_std.append(np.std(res_for_groups[n]["static_adj"]))

        half_acc_std.append(np.std(res_for_groups[n]["half_acc"]))
        half_adj_std.append(np.std(res_for_groups[n]["half_adj"]))

        max_acc_std.append(np.std(res_for_groups[n]["max_acc"]))
        max_adj_std.append(np.std(res_for_groups[n]["max_adj"]))

        random_acc_std.append(np.std(res_for_groups[n]["random_acc"]))
        random_adj_std.append(np.std(res_for_groups[n]["random_adj"]))

        oblivious_acc_std.append(np.std(res_for_groups[n]["oblivious_acc"]))
        oblivious_adj_std.append(np.std(res_for_groups[n]["oblivious_adj"]))

    print(groups)
    x = np.arange(len(groups))
    labels = groups
    width = 0.13  # the width of the bars: can also be len(x) sequence
    if q == "q3":
        width = 0.11
    fig, ax = plt.subplots()
    n_lines= 6
    c = np.arange(1, n_lines + 1)

    norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])
    color = ['0.9', '0.8', '0.7', '0.6', '0.5', '0.4', '0.3', '0.2', '0.1']
    if acc_only:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-')
        if stand_dev:
            ax.bar(x, rotate_acc_means, width, yerr=rotate_acc_std, label='Rotor_access', hatch=patterns[0], color='w', edgecolor='k',c='0.15')
            ax.bar(x + width, random_acc_means, width, yerr=random_acc_std, label='Random_access', hatch=patterns[1], color='w', edgecolor='k')
            ax.bar(x + width * 2, half_acc_means, width, yerr=half_acc_std, label='Half_access', hatch=patterns[2], color='w', edgecolor='k')
            ax.bar(x + width * 3, max_acc_means, width, yerr=max_acc_std, label='Max_access', hatch=patterns[3], color='w', edgecolor='k')
            ax.bar(x + width * 4, splay_acc_means, width, yerr=splay_acc_std, label='Splay_access', hatch=patterns[4], color='w', edgecolor='k')
            ax.bar(x + width * 5, oblivious_acc_means, width, yerr=oblivious_acc_std, label='Static_oblivious', hatch=patterns[5], color='w', edgecolor='k')
            ax.bar(x + width * 6, static_acc_means, width, yerr=static_acc_std, label='Static_opt', hatch=patterns[6], color='w', edgecolor='k')
        else:
            ax.bar(x, rotate_acc_means, width, label='Rotor_access', hatch=patterns[0], color=cmap.to_rgba(0 + 1), edgecolor='k')
            ax.bar(x + width, random_acc_means, width, label='Random_access', hatch=patterns[1],color=cmap.to_rgba(1 + 1),  edgecolor='k')
            ax.bar(x + width * 2, half_acc_means, width, label='Half_access', hatch=patterns[2],color=cmap.to_rgba(1.5 + 1),  edgecolor='k')
            ax.bar(x + width * 3, max_acc_means, width, label='Max_access', hatch=patterns[3], color=cmap.to_rgba(1.9 + 1), edgecolor='k')
            ax.bar(x + width * 4, splay_acc_means, width, label='Splay_access', hatch=patterns[4], color=cmap.to_rgba(2.5 + 1), edgecolor='k')
            ax.bar(x + width * 5, oblivious_acc_means, width, label='Static_oblivious', hatch=patterns[5], color=cmap.to_rgba(2.9 + 1), edgecolor='k')
            ax.bar(x + width * 6, static_acc_means, width, label='Static_opt', hatch=patterns[6], color=cmap.to_rgba(3.2 + 1), edgecolor='k')
    else:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
        if stand_dev:
            ax.bar(x, rotate_acc_means, width, yerr=rotate_acc_std, label='Rotor_access', hatch=patterns[0], color='w', edgecolor='k')
            ax.bar(x, rotate_adj_means, width, yerr=rotate_adj_std, label='Rotor_adjust', bottom=rotate_acc_means)

            ax.bar(x + width, random_acc_means, width, yerr=random_acc_std, label='Random_access', hatch=patterns[1], color='w', edgecolor='k')
            ax.bar(x + width, random_adj_means, width, yerr=random_adj_std, label='Random_adjust', bottom=random_acc_means)

            ax.bar(x + width * 2, half_acc_means, width, yerr=half_acc_std, label='Half_access', hatch=patterns[2], color='w', edgecolor='k')
            ax.bar(x + width * 2, half_adj_means, width, yerr=half_adj_std, label='Half_adjust', bottom=half_acc_means)

            ax.bar(x + width * 3, max_acc_means, width, yerr=max_acc_std, label='Max_access', hatch=patterns[3], color='w', edgecolor='k')
            ax.bar(x + width * 3, max_adj_means, width, yerr=max_adj_std, label='Max_adjust', bottom=max_acc_means)

            # ax.bar(x + width * 4, splay_acc_means, width, yerr=splay_acc_std, label='Splay_access', hatch=patterns[4], color='w', edgecolor='k')
            # ax.bar(x + width * 4, splay_adj_means, width, yerr=splay_adj_std, label='Splay_adjust', bottom=splay_acc_means)

            ax.bar(x + width * 5, oblivious_acc_means, width, yerr=oblivious_acc_std, label='Static_oblivious', hatch=patterns[5], color='w', edgecolor='k')

            ax.bar(x + width * 6, static_acc_means, width, yerr=static_acc_std, label='Static_opt', hatch=patterns[6], color='w', edgecolor='k')
        else:
            ax.bar(x, rotate_acc_means, width, label='Rotor_access', hatch=patterns[0], color='w', edgecolor='k')
            ax.bar(x, rotate_adj_means, width, label='Rotor_adjust', bottom=rotate_acc_means, hatch=patterns[7], color='w', edgecolor='k')

            ax.bar(x + width, random_acc_means, width, label = 'Random_access', hatch=patterns[1], color='w', edgecolor='k')
            ax.bar(x + width, random_adj_means, width, label='Random_adjust', bottom=random_acc_means, hatch=patterns[8], color='w', edgecolor='k')

            ax.bar(x + width * 2, half_acc_means, width, label='Half_access', hatch=patterns[2], color='w', edgecolor='k')
            ax.bar(x + width * 2, half_adj_means, width, label='Half_adjust', bottom=half_acc_means, hatch=patterns[9], color='w', edgecolor='k')

            # ax.bar(x + width * 3, splay_acc_means, width, label='Splay_access', hatch=patterns[3], color='w', edgecolor='k')
            # ax.bar(x + width * 3, splay_adj_means, width, label='Splay_adjust', bottom=splay_acc_means)

            ax.bar(x + width * 4, max_acc_means, width, label='Max_access', hatch=patterns[4], color='w', edgecolor='k')
            ax.bar(x + width * 4, max_adj_means, width, label='Max_adjust', bottom=max_acc_means, hatch=patterns[1], color='w', edgecolor='k')

            ax.bar(x + width * 5, oblivious_acc_means, width, label='Static_oblivious', hatch=patterns[5], color='w', edgecolor='k')

            ax.bar(x + width * 6, static_acc_means, width, label='Static_opt', hatch=patterns[6], color='w', edgecolor='k')

    ax.set_xticks(x + width * 3)
    ax.set_xticklabels(labels)
    ax.set_ylabel('Cost')
    # if acc_only:
    #     ax.set_title("Access, n = " + str(n_hosts) + ", r = " + str(n_req))
    # else:
    #     ax.set_title("Access + adjustment, n = " + str(n_hosts) + ", r = " + str(n_req))
    fig.set_figwidth(10)
    fig.set_figheight(10)
    #ax.legend(bbox_to_anchor=(0, -0.4), loc='lower left', borderaxespad=0., ncol=2, fontsize=22)
    #ax.legend(bbox_to_anchor=(0, -0.2), loc='lower left', borderaxespad=0., ncol=3, prop={'size': 15})
    #ax.legend(loc='lower left', borderaxespad=0.)
    filename = "plots/bar"
    if use_median:
        plot_type = "_median"
    else:  plot_type = "_mean"
    if q == "q1":
        filename += "_q1"
        if acc_only:
            filename += "_acc"
            ax.legend(bbox_to_anchor=(0, -0.3), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
        filename += "_noerr"
        plt.savefig(filename+plot_type+".pdf", bbox_inches='tight')
    elif q == "q1_fb":
        filename += "_q1_fb"
        if acc_only:
            filename += "_acc"
            ax.legend(bbox_to_anchor=(0, -0.3), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
        filename += "_noerr"
        plt.savefig(filename+plot_type+".pdf", bbox_inches='tight')
    elif q == "q2":
        filename += "_q2"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Temporal p')
        # if stand_dev:
        #     plt.savefig(filename+plot_type+".pdf", bbox_inches='tight')
        # else:
        filename += "_noerr"
        plt.savefig(filename+plot_type+".pdf", bbox_inches='tight')
    elif q == "q3":
        filename += "_q3"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Entropy')
        # if stand_dev:
        #     plt.savefig(filename+plot_type+".png", bbox_inches='tight')
        # else:
        filename += "_noerr"
        plt.savefig(filename+plot_type+".png", bbox_inches='tight')
    elif q == "q3_1":
        filename += "_q3_1"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Alpha')
        # if stand_dev:
        #     plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
        # else:
        filename += "_noerr"
        plt.savefig(filename + plot_type + ".png", bbox_inches='tight')
    elif q == "q3_2":
        filename += "_q3_2"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Zipf-a')
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "temp_incr_tree":
        filename += "_temp_incr_tree" + str(added_param)
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Tree size')
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    elif q == "zipf_incr_tree":
        filename += "_zipf_incr_tree"+str(added_param)
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Tree size', fontsize="17")
        plt.savefig(filename + plot_type + ".pdf", bbox_inches='tight')
    plot_bar_std_dev(q, acc_only, groups, rotate_acc_std, rotate_adj_std, random_acc_std, random_adj_std, half_acc_std,
                     half_adj_std, splay_acc_std, splay_adj_std, max_acc_std, max_adj_std,
                     oblivious_acc_std, oblivious_adj_std, static_acc_std, static_adj_std)

def plot_bar_q4(data, n_hosts, n_req, stand_dev):  # groups = temp_p, value_vars = algos
    groups = []
    res_for_groups = dict()
    for res in data:  # loop for each result (different experiments: temporal_p, entropy, real data, weibull)
        for a in res:  # loop for each algo
            one_algo = res[a]  # get all data for this algo
            for n in one_algo:
                res_for_net = one_algo[n]  # take res-list for specific p
                if n not in groups:
                    groups.append(n)
                    res_for_groups[n] = dict()  # dict of dicts
                if a == "rotate":
                    res_for_groups[n]["rotate_acc"] = []
                    res_for_groups[n]["rotate_adj"] = []
                    for i in range(0, len(res_for_net[5])):
                        for j in range(0, len(res_for_net[5][i])):
                            res_for_groups[n]["rotate_acc"].append(res_for_net[5][i][j])  # list cost of serving
                            res_for_groups[n]["rotate_adj"].append(res_for_net[3][i][j] + res_for_net[4][i][j])

    # each algo needs to have list of resp. means
    rotate_acc_means = []
    rotate_acc_std = []
    rotate_adj_means = []
    rotate_adj_std = []

    for n in res_for_groups:
        rotate_acc_means.append(sum(res_for_groups[n]["rotate_acc"]) / len(res_for_groups[n]["rotate_acc"]))
        rotate_acc_std.append(np.std(res_for_groups[n]["rotate_acc"]))
        rotate_adj_means.append(sum(res_for_groups[n]["rotate_adj"]) / len(res_for_groups[n]["rotate_adj"]))
        rotate_adj_std.append(np.std(res_for_groups[n]["rotate_adj"]))

    print(groups)
    x = np.arange(len(groups))
    labels = groups
    width = 0.15  # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()

    if stand_dev:
        ax.bar(x, rotate_acc_means, width, yerr=rotate_acc_std, label='Rotor_access', )
        ax.bar(x, rotate_adj_means, width, yerr=rotate_adj_std, label='Rotor_adjust', bottom=rotate_acc_means)
    else:
        ax.bar(x, rotate_acc_means, width, label='Rotor_access', )
        ax.bar(x, rotate_adj_means, width, label='Rotor_adjust', bottom=rotate_acc_means)

    ax.set_xticks(x + width / 2)
    ax.set_xticklabels(labels)
    ax.set_ylabel('Cost')
    #ax.set_title("Access + adjustment, n = " + str(n_hosts) + ", r = " + str(n_req))
    ax.legend(bbox_to_anchor=(0, -0.2), loc='lower left', borderaxespad=0., ncol=3)
    if stand_dev:
        plt.savefig("plots/bar_q4.png", bbox_inches='tight')
    else: plt.savefig("plots/bar_q4_noerr.png", bbox_inches='tight')

def plot_bar_std_dev(q, acc_only, groups, rotate_acc_std, rotate_adj_std, random_acc_std, random_adj_std, half_acc_std,
                     half_adj_std, splay_acc_std, splay_adj_std, max_acc_std, max_adj_std,
                     oblivious_acc_std, oblivious_adj_std, static_acc_std, static_adj_std):
    x = np.arange(len(groups))
    labels = groups
    width = 0.13  # the width of the bars: can also be len(x) sequence
    fig, ax = plt.subplots()
    n_lines = 6
    c = np.arange(1, n_lines + 1)

    norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.Blues)
    cmap.set_array([])
    #color = ['0.9', '0.8', '0.7', '0.6', '0.5', '0.4', '0.3', '0.2', '0.1']
    if acc_only:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-')
        ax.bar(x, rotate_acc_std, width, label='Rotor_access', hatch=patterns[0], color=cmap.to_rgba(0 + 1),
               edgecolor='k')
        ax.bar(x + width, random_acc_std, width, label='Random_access', hatch=patterns[1],
               color=cmap.to_rgba(1 + 1), edgecolor='k')
        ax.bar(x + width * 2, half_acc_std, width, label='Half_access', hatch=patterns[2],
               color=cmap.to_rgba(1.5 + 1), edgecolor='k')
        ax.bar(x + width * 3, max_acc_std, width, label='Max_access', hatch=patterns[3],
               color=cmap.to_rgba(1.9 + 1), edgecolor='k')
        if splay_acc_std:
            ax.bar(x + width * 4, splay_acc_std, width, label='Splay_access', hatch=patterns[4],
                   color=cmap.to_rgba(2.5 + 1), edgecolor='k')
            ax.bar(x + width * 5, oblivious_acc_std, width, label='Static_oblivious', hatch=patterns[5],
                   color=cmap.to_rgba(2.9 + 1), edgecolor='k')
            ax.bar(x + width * 6, static_acc_std, width, label='Static_opt', hatch=patterns[6],
                   color=cmap.to_rgba(3.2 + 1), edgecolor='k')
        else:
            ax.bar(x + width * 4, oblivious_acc_std, width, label='Static_oblivious', hatch=patterns[5],
                   color=cmap.to_rgba(2.9 + 1), edgecolor='k')
            ax.bar(x + width * 5, static_acc_std, width, label='Static_opt', hatch=patterns[6],
                   color=cmap.to_rgba(3.2 + 1), edgecolor='k')
    else:
        patterns = ('/', '.', '\\', 'o', '*', 'x', 'O', '-', '+', '|')
        ax.bar(x, rotate_acc_std, width, label='Rotor_access', hatch=patterns[0], color=cmap.to_rgba(0 + 1),
               edgecolor='k')
        ax.bar(x, rotate_adj_std, width, label='Rotor_adjust', bottom=rotate_acc_std, hatch=patterns[2],
               color=cmap.to_rgba(0 + 1), edgecolor='k')

        ax.bar(x + width, random_acc_std, width, label='Random_access', hatch=patterns[1], color=cmap.to_rgba(1 + 1),
               edgecolor='k')
        ax.bar(x + width, random_adj_std, width, label='Random_adjust', bottom=random_acc_std, hatch=patterns[6],
               color=cmap.to_rgba(1 + 1), edgecolor='k')

        ax.bar(x + width * 2, half_acc_std, width, label='Half_access', hatch=patterns[2],
               color=cmap.to_rgba(1.5 + 1), edgecolor='k')
        ax.bar(x + width * 2, half_adj_std, width, label='Half_adjust', bottom=half_acc_std, hatch=patterns[0],
               color=cmap.to_rgba(1.5 + 1), edgecolor='k')

        # ax.bar(x + width * 3, splay_acc_std, width, label='Splay_access', hatch=patterns[3], color='w', edgecolor='k')
        # ax.bar(x + width * 3, splay_adj_std, width, label='Splay_adjust', bottom=splay_acc_std)

        ax.bar(x + width * 3, max_acc_std, width, label='Max_access', hatch=patterns[3], color=cmap.to_rgba(1.9 + 1),
               edgecolor='k')
        ax.bar(x + width * 3, max_adj_std, width, label='Max_adjust', bottom=max_acc_std, hatch=patterns[1],
               color=cmap.to_rgba(1.9 + 1), edgecolor='k')

        ax.bar(x + width * 4, oblivious_acc_std, width, label='Static_oblivious', hatch=patterns[5],
               color=cmap.to_rgba(2.5 + 1), edgecolor='k')

        ax.bar(x + width * 5, static_acc_std, width, label='Static_opt', hatch=patterns[6],
               color=cmap.to_rgba(2.9 + 1), edgecolor='k')

    ax.set_xticks(x + width * 3)
    ax.set_xticklabels(labels)
    ax.set_ylabel('std-dev')
    # if acc_only:
    #     ax.set_title("Access, n = " + str(n_hosts) + ", r = " + str(n_req))
    # else:
    #     ax.set_title("Access + adjustment, n = " + str(n_hosts) + ", r = " + str(n_req))
    fig.set_figwidth(10)
    fig.set_figheight(10)
    filename = "plots/std_dev"
    if q == "q1":
        filename += "_q1"
        if acc_only:
            filename += "_acc"
            ax.legend(bbox_to_anchor=(0, -0.3), loc='lower left', borderaxespad=0., ncol=2, fontsize=19)
        # if stand_dev:
        #     plt.savefig(filename+plot_type+".png", bbox_inches='tight')
        # else:
        filename += "_noerr"
        plt.savefig(filename+".pdf", bbox_inches='tight')
    elif q == "q2":
        filename += "_q2"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Temporal p')
        plt.savefig(filename+".pdf", bbox_inches='tight')
    elif q == "q3":
        filename += "_q3"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Entropy')
        plt.savefig(filename+".png", bbox_inches='tight')
    elif q == "q3_1":
        filename += "_q3_1"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Alpha')
        plt.savefig(filename + ".png", bbox_inches='tight')
    elif q == "q3_2":
        filename += "_q3_2"
        if acc_only:
            filename += "_acc"
        ax.set_xlabel('Zipf-a')
        plt.savefig(filename + ".pdf", bbox_inches='tight')

def plot_rot_vs_rand(size_dataset):
    filename = "plots/rot-rand_all_data.json"
    seq_length = size_dataset

    with open(filename, "r") as handle:
        data = json.load(handle)

    sequence = []

    for k in range(1, 11):  # this line was added for the icdcs22 submission
        for i in range(seq_length):
            rotate_cost = sum([data[str(k)]['rotate']['0'][str(i)][key] for key in ['access']])
            random_cost = sum([data[str(k)]['random']['0'][str(i)][key] for key in ['access']])
            sequence.append(rotate_cost - random_cost)

    mean = np.round(np.mean(sequence), decimals=4)

    bins = {x for x in sequence}
    n_bins = len(bins)

    fig, axs = plt.subplots(1, sharey=True, tight_layout=True)

    # We can set the number of bins with the `bins` kwarg
    # axs[0].hist(range(len(sequence)), bins=n_bins)

    plt.yscale('log')

    plotfontsize = 14

    plt.rcParams.update({'font.size': plotfontsize})

    plt.axvline(x=mean, label='mean', c='r')

    axs.hist(sequence, bins=n_bins, density=True)

    axs.set_xlabel(f'access cost difference (mean = {mean})', fontsize=plotfontsize)
    axs.set_ylabel('probability', fontsize=plotfontsize)

    plt.xticks(size=plotfontsize)
    plt.yticks(size=plotfontsize)

    plt.savefig("plots/rotor-random-access-only.pdf", bbox_inches='tight')