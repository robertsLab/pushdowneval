import simulation.artificial_experiment as ae
import simulation.multi_experiment as mx
from pushDown.half_move_opt import half_move_opt
from pushDown.minMaxPush import maxPush
from pushDown.randomPush import randomPush
from pushDown.treeStructure import CompleteTree, Item
from simulation.data_generator import generate_zipf_sequences, generate_zipf_for_n


def main():
    # data-generation
    #generate_zipf_sequences()     # generate datasets before running Q1, Q3, Q4

    # paper experiments
    # Q1: incrementing tree size experiments
    #ae.run_temporal_incr_tree()
    #ae.run_incr_tree_zipf()

    # Q2
    # params = [10, 65535, 1000000]
    # ae.set_params_q2(params)
    # ae.create_increasing_p_data()     # optional, in case we want to generate sequences separately
    # ae.run_art_increasing_p()

    # Q3
    #ae.run_incr_zipf()

    # Q4
    #ae.run_incr_rotor_vs_obl_using_zipf()   # Fig. 5a
    #ae.run_rotor_vs_random()                # Fig. 5b

    # Q5
    # mx.run_fix_net_calgary()

    # Simple testing as demo
    a, b, c, d, e, x, y, z = Item(1), Item(2), Item(3), Item(4), Item(5), Item(6), Item(7), Item(8)
    arr = [a, b, c, d, e, x, y, z]
    ct1 = CompleteTree()
    for x in arr:
        ct1.insert(x)
    p = randomPush(ct1)

    p.access(6)
    p.access(2)



if __name__ == '__main__':
    main()


    
